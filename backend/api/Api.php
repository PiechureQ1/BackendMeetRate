<?php
namespace backend\api;

use backend\controllers\CookiesController;
use backend\cutCookie\cutCookies;
use common\models\LoginForm;
use skeeks\yii2\curl\Curl;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: rst_user_6
 * Date: 09.07.2018
 * Time: 11:58
 */
class Api{


    private $baseUrl;

    public function __construct($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public $cookies;

    /**
     * @param $endpoint
     * @param $array
     * @param $cookie
     * @return string
     * @internal param $url
     */
    public function post($endpoint, $array, $cookie){

        $this->curl = new Curl();
        $url = $this->baseUrl.$endpoint;

        $json = json_encode($array);


        $this->curl->setOption(CURLOPT_HTTPHEADER, [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $cookie",

        ])->setOption(CURLOPT_POSTFIELDS, $json)->post($url);

        if($this->curl->responseCode >= 500){
            $validation = new CookiesController();
            $validation->deadApi();
        }
        return $this->curl;

    }


    public function curlResponseHeaderCallback($ch, $headerLine) {
        if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) == 1)
            $this->cookies[] = $cookie;
        return strlen($headerLine); // Needed by curl
    }


    private $returnObject = null;

    public function resturnAs($className) {
        $this->returnObject = new $className();

        return $this;
    }

    private $curl;



    public function getResponse() {
        return $this->curl->response;
    }

    /**
     * @param $password
     * @param $salt
     * @return Curl
     */

    public function passwordHash($password, $salt){
        $password_hash = crypt($password, $salt);
        return $password_hash;
    }

    public function login($email, $password_hash)  {

        $url = $this->baseUrl.'login';

        $this->curl = new Curl();

        $this->curl->setOption(CURLOPT_HEADER, 1);
        $this->curl->setOption(CURLOPT_HEADERFUNCTION, [$this, "curlResponseHeaderCallback"]);
        $this->curl->setOption(CURLOPT_HTTPHEADER, [
            "email: $email",
            "password_hash: $password_hash"
        ])->get($url);

        if($this->curl->responseCode >= 500){
            $validation = new CookiesController();
            $validation->deadApi();
        }
        return $this->curl;
    }

    public function signUp($username ,$email, $type, $cookie)  {

        $this->curl = new Curl();
        $url = $this->baseUrl.'user';

        $array = [
            'name' => $username,
            'email' => $email,
            'type' => $type,
        ];
        $json = json_encode($array);


        $this->curl->setOption(CURLOPT_HTTPHEADER, [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $cookie",

        ])->setOption(CURLOPT_POSTFIELDS, $json)->post($url);

        if($this->curl->responseCode >= 500){
            $validation = new CookiesController();
            $validation->deadApi();
        }
        return $this->curl;
    }

    public function postTalk($title, $start, $end, $speakers_array, $eventId, $cookie){

    $this->curl = new Curl();
    $url = $this->baseUrl.'talk';

    $start = strtotime($start);
    $end = strtotime($end);

    $array = [
        'eventId' => $eventId,
        'title' => $title,
        'date' => [
            'start' => $start,
            'end' => $end,
        ],
        'speakers' => $speakers_array,
    ];
    $json = json_encode($array);

    $this->curl->setOption(CURLOPT_HTTPHEADER, [
        "Content-Type: application/json; charset=UTF-8",
        "Cookie: $cookie",

    ])->setOption(CURLOPT_POSTFIELDS, $json)->post($url);

        if($this->curl->responseCode >= 500){
            $validation = new CookiesController();
            $validation->deadApi();
        }
        return $this->curl;

}

    public function sendCookie($cookie){

        $this->curl = new Curl();
        $url = $this->baseUrl.'logout';

        $this->curl->setOption(CURLOPT_HTTPHEADER, [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $cookie",

        ])->get($url);

    }


    public function get($endpoint, $headers = [], $queryParams = [], $curlOptions = []) : Curl {

        $url = $this->baseUrl.$endpoint;

        $queryParams = \GuzzleHttp\Psr7\build_query($queryParams);

        if($queryParams) {
            implode("?", [$url, $queryParams]);
        }

        $this->curl = new Curl();
        foreach ($curlOptions as $option => $value) {
            $this->curl->setOption($option, $value);
        }

        $this->curl->setOption(CURLOPT_HTTPHEADER, $headers)->get($url);
        if($this->curl->responseCode >= 500){
            $validation = new CookiesController();
            $validation->deadApi();
        }
            return $this->curl;
    }

    public function returnAsObject($className){
        $ro = new $className;

        if(empty($ro)) {
            return $this->curl;
        }

        $jsonData = json_decode($this->curl->response);

        foreach ($jsonData as $key => $value) {
            $ro->$key = $value;
        }

        return $ro;
    }

    public function put($url, $array){

        $curl = new Curl();
        try {
            $result = $curl->setOption(CURLOPT_PUT, http_build_query($array))->put($url);
        } catch (Exception $e) {
            return [$e->getMessage()];
        }

        return $result;
    }

    /**
     * @param $url
     * @param $cookie
     * @return Curl
     * @throws Exception
     */
    public function delete($url, $cookie){

        $this->curl = new Curl();
        $url = $this->baseUrl.$url;


        $this->curl->setOption(CURLOPT_HTTPHEADER, [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $cookie",

        ])->delete($url);

        if($this->curl->responseCode >= 500){
            $validation = new CookiesController();
            $validation->deadApi();
        }
        return $this->curl;
    }

    /**
     * @param $endpoint
     * @param $array
     * @param $cookie
     * @return Curl
     * @throws Exception
     */
    public function patch($endpoint, $array, $cookie){

        $this->curl = new Curl();
        $url = $this->baseUrl.$endpoint;

        $json = json_encode($array);

        $this->curl->setOption(CURLOPT_HTTPHEADER, [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $cookie",

        ])->setOption(CURLOPT_POSTFIELDS, $json)->patch($url);

        if($this->curl->responseCode >= 500){
            $validation = new CookiesController();
            $validation->deadApi();
        }
        return $this->curl;

    }

    /**
     * @return mixed
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @param mixed $curl
     */
    public function setCurl($curl)
    {
        $this->curl = $curl;
    }


}