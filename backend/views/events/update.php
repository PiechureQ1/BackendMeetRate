<?php
use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/**
 * Created by PhpStorm.
 * User: rst_user_1
 * Date: 16.07.2018
 * Time: 13:02
 */
$this->title = 'Aktualizacja wydarzenia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

                <?php $form = ActiveForm::begin(['id' => 'Signup-form','options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'name') ?>
                        <?= $form->field($model, 'info')->textarea(['rows' => '6']) ?>
                        <div class="col-12" style="padding:0px!important">
                        <?= $form->field($model, 'start')->widget(DateTimePicker::class, [
                            'type' => DateTimePicker::TYPE_INPUT,
                    'options' => ['placeholder' => 'Początek wydarzenia','readonly' => true],
                    'pluginOptions' => ['autoclose' => true, 'startDate' => date("Y-m-d H:i")]]); ?>
                        </div>
                        <div class="col-12" style="padding:0px!important">
                        <?= $form->field($model, 'end')->widget(DateTimePicker::class, [
                            'type' => DateTimePicker::TYPE_INPUT,
                    'options' => ['placeholder' => 'Koniec wydarzenia','readonly' => true],
                    'pluginOptions' => ['autoclose' => true, 'startDate' => date("Y-m-d H:i")]]); ?>
                        </div>
                        <?php
                        echo '<label class="control-label">Logo wydarzenia</label>';
                        echo FileInput::widget([
                            'name' => 'img',
                            'pluginOptions'=>[
                                    'showCancel' => false,
                                    'showUpload' =>false
                            ]
                        ]);
                        ?>
                    </div>



                    <div class="col-lg-6">
                        <?=
                        $form->field($model, 'fullLocation')->widget(\kalyabin\maplocation\SelectMapLocationWidget::class, [
                            'attributeLatitude' => 'lat',
                            'attributeLongitude' => 'lng',
                            'googleMapApiKey' => 'AIzaSyDNQA-S5FN7HNz9Q7RPL5zSZD3yj8vwebE',
                            'draggable' => true,
                        ]); ?>
                        <div class="popup sadPrompt">
                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="alert alert-error alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cancel"></i></button>
                                    <h4><i class="icon-icon-alert"></i>Błąd</h4>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php endif; ?>
                        </div>

                        
                        <div class="form-btn">
                            <?= Html::submitButton('Zatwierdź', ['class' => 'btn btn-info add-event-btn', 'name' => 'create-talk-button']) ?>
                        </div>
                    </div>
                    </div>
                

                <?php ActiveForm::end(); ?>
        </div>