<?php

use backend\api\Api;
use backend\controllers\CookiesController;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \backend\models\EventsForm*/

$this->title = 'Wydarzenia';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    table, th, td {
        border-collapse: collapse;
        overflow-x:auto ;
    }
    table{
        margin-top:30px;
    }
    th, td {
        font-weight:600;
        padding: 5px;
        margin: 10px;
        text-align: center;
        color:var(--dark-c);
        border-bottom: 3px solid var(--dark-c);
        border-top: 1px solid var(--dark-c);
        border-left: 1px solid var(--dark-c);
        border-right: 1px solid var(--dark-c);
        background:white;
    }
    .add-btn{
        margin-top:20px;
        width:100px;
    }
</style>
</head>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

     <?= Html::a('Dodaj', ['create'], ['class' => 'btn btn-info add-event-btn']) ?>
     
    <div class="tabelka">
    <?php
            $api = new Api(Yii::$app->params['apiDomain']);
            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');
            $userData = $cookie->getValue('user');

    if($sid == null){
        //validation
        $validation = new CookiesController();
        $validation->CookiesValidation();
    }else {

        if($userData['type'] == 3){
            $curl = $api->get('event', [
                "Content-Type: application/json; charset=UTF-8",
                "Cookie: $sid",]);
        }else{
            $curl = $api->get('event/user/'. $userData['userId'], [
                "Content-Type: application/json; charset=UTF-8",
                "Cookie: $sid",]);
        }
        $event = json_decode($curl->response, true);
        if($event == null){
            $size = 0;
        }else{
            $size = count($event);
        }

        echo '<table style="width:100%;">
    <tr>
    <th>Nazwa</th>
    <th>Miasto</th> 
    <th>Miejsce</th>
    <th>Początek</th> 
    <th>Koniec</th>
    <th>Status</th>
    <th>Średnia ocen</th>
    <th>Ilość głosów</th>
    <th style="width:150px;min-width:150px;">Operacje</th>
  </tr>';
        for ($i = 0; $i < $size - 1; $i++) {
            $a = $event[$i];
            $location = $a['location'];
            $date = $a['date'];
            $feedBack = $a['feedback'];

            if($a['status'] == 'archive'){
                $status = 'Zakończony';
            }elseif ($a['status'] == 'future'){
                $status = 'Przyszły';
            }elseif ($a['status'] == 'actual'){
                $status = 'W trakcie';
            }else{
                $status = 'BŁĄD!';
            }
            date_default_timezone_set('Europe/Warsaw');
            $dateStart = date('Y-m-d H:i:s', $date['start']);
            $dateEnd = date('Y-m-d H:i:s', $date['end']);

            echo '<tr>
            <td>' . $a['name'] . '</td>
            <td>' . $location['city'] . '</td>
            <td>' . $location['place'] . '</td> 
            <td>' . $dateStart . '</td>
            <td>' . $dateEnd . '</td> 
            <td>' . $status . '</td>
            <td>' . round($feedBack['avg'], 2) . '/5' . '</td> 
            <td>' . $feedBack['count'] . '</td>
            <td>' .Html::a('<i class="icon-trash-empty col-6"></i>', \yii\helpers\Url::to(['delete-event', 'id' => $a['_id'], 'sid' => $sid]), ['class' => 'btn   sadPrompt float justTwo',
                    'data' => [
                        'confirm' => 'Czy napewno chcesz skasować to wydarzenie?',
                        'method' => 'post',]]) .'
                '.Html::a('<i class="icon-search col-6"></i>', \yii\helpers\Url::to(['view-event', 'id' => $a['_id']]), ['class' => 'btn  happyPrompt float justTwo']);
            $current_date = strtotime(date('Y-m-d H:i'));
            if($date['start'] >= $current_date || $date['end'] >= $current_date){
                echo Html::a('<i class="icon-pencil col-4"></i>', \yii\helpers\Url::to(['update-event', 'id' => $a['_id']]), ['class' => 'btn blasePrompt float']) . '
                </td></tr>';
            }else{
                echo '</td></tr>';
            }


        }
        echo '</table>';

    }?>
    </div>
</div>

<div class="popup happyPrompt">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-error alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cancel"></i></button>
                        <h4><i class="icon-icon-alert"></i>Sukces!</h4>
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                    <?php endif; ?>
            </div>