<?php
/**
 * Created by PhpStorm.
 * User: rst_user_1
 * Date: 17.07.2018
 * Time: 08:25
 */

use yii\bootstrap\ActiveForm;
use backend\api\Api;
use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = 'Podgląd wydarzenia';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table, th, td {
        border-collapse: collapse;
        overflow-x:auto ;
    }
    table{
        margin-top:30px;
    }
    th, td {
        font-weight:600;
        padding: 5px;
        margin: 10px;
        text-align: center;
        color:var(--dark-c);
        border-bottom: 3px solid var(--dark-c);
        border-top: 1px solid var(--dark-c);
        border-left: 1px solid var(--dark-c);
        border-right: 1px solid var(--dark-c);
        background:white;
    }
    .add-btn{
        margin-top:20px;
        width:100px;
    }
    .rate-event {
    background: -webkit-linear-gradient(180deg, #34c868, #11856e);
    color: white;
    cursor: pointer;
    padding: 10px;
    width: 100%;
    margin-top:28px;
    border-radius: 5px 5px 0px 0px;
    border: none;
    text-align: center;
    outline: none;
    font-size: 17px;
    transition: 0.4s;
}

.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}
.dropbtn:hover{
    box-shadow: 0px 2px 23px 2px rgba(17, 133, 110,.3);
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: white;
    min-width: 230px;
    overflow: auto;
    border-left: 5px solid #11856e;
    border-right: 5px solid #34c868;
    border-bottom: 5px solid #11856e;
    z-index: 1;
    padding: 0px;
    max-height: 300px;
    overflow-y: auto;
}
.showRate{display: block;}

.event-rate{
    border-bottom: 2px solid  #11856e;
    margin:0px;
    padding:4px;
    overflow:scroll;
    max-width:100%;
    max-height:100px;
    position: relative;
    overflow-y:hidden;
    max-height:50%;
    min-height:80px;
}
h1{
    margin:0px;
}
.rate, .comment, .event-rate h1{
    float:left;
}
.rate{
    height:100% !important;
    padding: 10px;

}
.non-rate{
    text-align:center;
}
.non-rate h1{
    font-size:18px!important;
    margin-top:10px;
    text-align:center;
    margin-left: calc(50% - 165px);
}
h5{
    font-size:16px!important;
    text-align:justify!important;
    padding:5px;

}
h5:after{
    content: "";
    clear:both;
}
.view-event-img{
    max-width:100%;
    max-height: 300px;
    margin: 30px auto 0px auto;
    display:block;
}
.rate{
    float:left;
    position: absolute;
    top:0px;
    left:0px;
}
.comment{
    float:left;
    padding : 10px;
    text-align:justify;
    overflow-x:scroll;
    margin-left:65px;
}
.rate-date{
    position: absolute;
    bottom:-5px;
    right:10px;
    float:left;
}
.rate-date:after{
    content:"";
    clear:both;
}
.non-rate h1{
    text-align:center!important;
    float:none;
}


</style>
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'view-form']); ?>

                <?= $form->field($model, 'name')->textInput(['readonly' => true]) ?>
                <?= $form->field($model, 'info')->textarea(['rows' => '6', 'readonly' => true])?>
                <?= $form->field($model, 'start')->textInput(['readonly' => true]) ?>
                <?= $form->field($model, 'end')->textInput(['readonly' => true]) ?>
                <?=
                $form->field($model, 'fullLocation')->widget(\kalyabin\maplocation\SelectMapLocationWidget::class, [
                    'attributeLatitude' => 'lat',
                    'attributeLongitude' => 'lng',
                    'googleMapApiKey' => 'AIzaSyDNQA-S5FN7HNz9Q7RPL5zSZD3yj8vwebE',
                    'draggable' => false,
                ])->textInput(['readonly' => true]); ?>

            <?php ActiveForm::end(); ?>
        </div>
        <?php
        $api = new Api(Yii::$app->params['apiDomain']);
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');
        $curl = $api->get('event/'. $_GET['id'],[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);

        $rating = json_decode($curl->response,true);
        ?>

        

        <div class="col-lg-6">
        <img class="view-event-img" src='data:image/png;base64, <?= $rating["img"] ?>'/>

            <div class="dropdown">
                <button onclick="myFunction()" class="dropbtn col-12 rate-event"><h3>Oceny wydarzenia</h3></button>
                <div id="myDropdown" class="dropdown-content col-12">

                    <?php
                $rating = $rating['feedback'];
                $rating_size = count($rating);
                if($rating_size == 1){
                   
                    echo '<div class="event-rate non-rate just-rate">
                                <h1>Wydarzenie nie zostało jeszcze ocenione</h1>
                          </div>';
                }

                for($i = 0; $i < $rating_size-1; $i++){
                    $rating_one = $rating[$i];
                    $date = date('d-m-Y H:i',$rating_one['date']);
                    if($rating_one['content'] != null){
                        echo '<div class="event-rate">
                                    <div  class="rate">
                                        <h1>'. $rating_one["rating"] .'/5</h1>
                                    </div>
                                    <div class="comment">
                                        '. $rating_one['content'] .'
                                    </div>
                                    <div class="rate-date">
                                        <h6 class="date-rate" >'. $date .'</h6>
                                    </div>
                                </div>';
                    }else{
                        echo '<div class="event-rate">
                                    <div  class="rate">
                                        <h1>'. $rating_one["rating"] .'/5</h1>
                                    </div>
                                    <div class="rate-date">
                                        <h6 class="date-rate" >'. $date .'</h6>
                                    </div>
                                </div>';
                    }
                }
                ?>

            </div>
        </div>

        <script>
            function myFunction() {
            document.getElementById("myDropdown").classList.toggle("showRate");
            }
        </script>


        <?php
        $api = new Api(Yii::$app->params['apiDomain']);
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');
        $talksDate = $api->get('talk/event/'.$_GET['id'],[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $talk = json_decode($talksDate->response, true);
        $size = count($talk);


        $eventDate = $api->get('event/'. $_GET['id'],[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $eventDate = json_decode($eventDate->response,true);

        $current_date = strtotime(date('Y-m-d H:i'));
        if($eventDate['date']['start'] >= $current_date || $eventDate['date']['start'] >= $current_date){
            echo Html::a('Dodaj', \yii\helpers\Url::to(['/talks/create', 'eventId'=>$_GET['id']]), ['class'=>'btn add-event-btn btn-info']);
        }


        echo   '<div class="tabelka">';
        echo '<table style="width:100%">
    <tr>
    <th>Tytuł prelekcji</th>
    <th>Prelegenci</th> 
    <th>Poczatek prelekcji</th> 
    <th>Koniec prelekcji</th>
    <th style="width:150px;min-width:150px;">Operacje</th>
  </tr>';
        for($i = 0;$i<$size;$i++) {
            $a = $talk[$i];
            $date = $a['date'];
            $speaker = $a['speakers'];
            $speaker_size = count($speaker);

            date_default_timezone_set('Europe/Warsaw');

            $date['start'];
            $dateStart = date('Y-m-d H:i:s',$date['start']);
            $dateEnd = date('Y-m-d H:i:s',$date['end']);

            for($c = 0;$c < $speaker_size;$c++){

                $get_speaker = $api->get('user/' . $speaker[$c], [
                    "Content-Type: application/json; charset=UTF-8",
                    "Cookie: $sid",]);


                $speaker_json = $get_speaker->response;
                $ar = json_decode($speaker_json);
                $speaker_array = json_decode(json_encode($ar), true);
                $speaker_name = $speaker_array['name'];
            }



            echo '<tr><td>' . $a['title'] . '</td><td> ';

            for($c = 0;$c < $speaker_size;$c++){

                $get_speaker = $api->get('user/' . $speaker[$c], [
                    "Content-Type: application/json; charset=UTF-8",
                    "Cookie: $sid",]);


                $speaker_json = $get_speaker->response;
                $ar = json_decode($speaker_json);
                $speaker_array = json_decode(json_encode($ar), true);
                $speaker_name = $speaker_array['name'];
                echo $speaker_array['name']."</br>";
            }


            echo '
                </td> 
                 <td>' . $dateStart . '</td>
                 <td>' . $dateEnd . '</td>
                <td>
                '. Html::a('<i class="icon-trash-empty col-4"></i>', \yii\helpers\Url::to(['../talks/delete-talks', 'id' => $a['_id'], 'sid' => $sid, 'eventId' => $_GET['id']]), [
                    'class' => 'btn   sadPrompt float',
                    'data' => [
                        'confirm' => 'Czy napewno chcesz skasować tę prelekcję?',
                        'method' => 'post',]]) .'
                '.Html::a('<i class="icon-search col-4"></i>', \yii\helpers\Url::to(['../talks/view-talks', 'id' => $a['_id']]), ['class' => 'btn  happyPrompt float']);
            if($eventDate['date']['start'] >= $current_date || $eventDate['date']['start'] >= $current_date) {
                echo Html::a('<i class="icon-pencil col-4"></i>', \yii\helpers\Url::to(['../talks/update-talks', 'id' => $a['_id'], 'eventId' => $_GET['id']]), ['class' => 'btn blasePrompt float']) . '
             </td></tr>';
            }else{
                echo '</td></tr>';
            }

        }
        echo '</table>';
        echo '</div>';
        ?>


        </div>
    </div>

    <div class="popup happyPrompt">
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-error alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cancel"></i></button>
        <h4><i class="icon-icon-alert"></i>Sukces!</h4>
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>
    </div>


