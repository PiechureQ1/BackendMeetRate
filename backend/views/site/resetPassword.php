<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Zmień hasło';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    

    <div class="row justify-content-md-center">
        
        <div class="col-12 col-sm-6" style="margin-top:30px;">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'new_password')->passwordInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'repeat_password')->passwordInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Zapisz', ['class' => 'btn btn-info add-event-btn']) ?>
                </div>

            <div class="popup sadPrompt">
                <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-error alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cancel"></i></button>
                    <h4><i class="icon-icon-alert"></i>Błąd</h4>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
                <?php endif; ?>
            </div>



            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
