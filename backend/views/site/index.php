<?php

/* @var $this yii\web\View */

use backend\api\Api;
use backend\controllers\CookiesController;

$this->title = 'Strona główna';
?>

<?php
    $api = new Api(Yii::$app->params['apiDomain']);
    $cookie = Yii::$app->request->cookies;
    $sid = $cookie->getValue('sid');
    $userData = $cookie->getValue('user');

if($sid == null){
    //validation
    $validation = new CookiesController();
    $validation->CookiesValidation();
}else {


    if($userData['type'] == 3){
        $curl = $api->get('event', [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
    }else{
        $curl = $api->get('event/user/'. $userData['userId'], [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
    }

    $array = json_decode($curl->response, true);

    $event_size = count($array);
    $event_size = $event_size-1;



    $user = $api->get('user', [
        "Content-Type: application/json; charset=UTF-8",
        "Cookie: $sid",]);
    $json = $user->response;
    $array = json_decode($json);
    $y = json_decode(json_encode($array), true);
    $user_size = count($y);

    echo $this->render('menu-items', [
        'event_size' => $event_size,
        'user_size' => $user_size
    ]);

    }


?>


<!-- <div class="row justify-content-around">
  <a id="id-for-tester-4" href="#" class="col-12 col-sm-5 col-md-4 col-lg-3 box1 h-sm-100"><span id="num-of-rating">72</span><br> oceny </a>
  <a id="id-for-tester-5" href="#" class="col-12 col-sm-5 col-md-4 col-lg-3 box1 h-sm-100"><span id="num-of-comments">1</span><br> komentarz</a>
   
</div> -->
