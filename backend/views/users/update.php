<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\UsersForm */

use backend\api\Api;
use backend\controllers\CookiesController;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Zaktualizuj informacje o użytkowniku';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row justify-content-center" style="margin-top:40px;">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'email') ?>

            <?php
            $api = new Api(Yii::$app->params['apiDomain']);
            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');
            $userData = $cookie->getValue('user');

            if($sid == null){
                //validation
                $validation = new CookiesController();
                $validation->CookiesValidation();
            }else {
                if($userData['type'] == 3){
                    echo $form->field($model, 'type')->widget(Select2::class, [
                        'data' => ['3' => 'superadmin', '2' => 'admin', '1' => 'prelegent'],
                        'options' => ['placeholder' => 'Wybierz poziom uprawnień ...'],
                        'pluginOptions' => [
                            'allowClear' => false
                        ]]);
                }else{
                    echo $form->field($model, 'type')->widget(Select2::class, [
                        'data' => ['1' => 'prelegent'],
                        'options' => ['placeholder' => 'Wybierz poziom uprawnień ...'],
                        'pluginOptions' => [
                            'allowClear' => false
                        ]]);
                }


            }?>

            <div class="popup sadPrompt">
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-error alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cancel"></i></button>
                        <h4><i class="icon-icon-alert"></i>Błąd</h4>
                        <?= Yii::$app->session->getFlash('error') ?>
                    </div>
                    <?php endif; ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Zatwierdź', ['class' => 'btn btn-info add-event-btn', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
