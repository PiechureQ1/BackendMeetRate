<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
AppAsset::register($this);
if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}
$this->params['breadcrumbs'] = [];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    

    
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="/fontello/code.js"></script>
    <link href="/fontello/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/fontello/css/meetrate-icons.css">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="icon" 
      type="image/png" 
      href="/fontello/favicon.ico">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<?php if(isset($_GET['c'])) : ?>

<style>
body {
    background: url('/fontello/charlie-hunnam.jpg') no-repeat center center fixed; ;
    background-size: cover;
}

</style>

<?php endif; ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    
    <nav class="closed">
    <div class="close-open-btn">
        <i id="icon" class="icon-menu" onclick="moveMenu()"></i>
      
    </div>
        <a href="https://rst.com.pl/" target="_blank"><div class="company" style="padding-top:16px;"><img src="/fontello/rst-logo.png"></div></a>
        <div class="company"><img src="/fontello/meetrate-logo.png"></div>
        <div style="clear:both;"></div>
        <div class="user">Administrator</div>

        <ul class="main-nav">
            <li><a href="/" class="show"><i class="icon-home"></i> Strona główna</a></li><div style="clear:both;"></div>
            <li><a href="/events/index" class="show"><i class="icon-users"></i> Wydarzenia</a><a href="/events/create" class="add"><i class="icon-plus"></i></a></li><div style="clear:both;"></div>
            <li><a href="/users/index" class="show"><i class="icon-user"></i> Użytkownicy</a><a href="/users/create" class="add"><i class="icon-plus"></i></a></li><div style="clear:both;"></div>
            <li><a href="/site/reset-password" class="show"><i class="icon-lock"></i> Zmień hasło</a></li><div style="clear:both;"></div>
            <li><a href="/site/logout" class="show"><i class="icon-power"></i> Wyloguj</a></li><div style="clear:both;"></div>
        </ul>
    </nav>
    
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
