<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 12.07.2018
 * Time: 17:41
 */

namespace backend\controllers;


use backend\api\Api;
use backend\models\UsersForm;
use Yii;
use yii\web\Controller;
use backend\controllers\CookiesController;

class UsersController extends Controller
{

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCreate()
    {
        $validation = new CookiesController();
        $validation->CookiesValidation();

        $model = new UsersForm();
        if ($model->load(Yii::$app->request->post())) {

            $username = explode(' ',$model->username);
            $username = count($username);
            if($username == 1){
                $model->addError('username', 'Nazwa użytkownika musi zawierać imię i nazwisko');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }



            $api = new Api(Yii::$app->params['apiDomain']);

            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');

            $curl = $api->signUp($model->username,$model->email, $model->type, $sid);



            if($curl->responseCode == 201){
                Yii::$app->getSession()->setFlash('success', 'Dodano użytkowniaka');
                return $this->redirect('index');
            }elseif($curl->responseCode == 400){
                Yii::$app->getSession()->setFlash('error', 'Złe dane');
            }elseif($curl->responseCode == 409){
                Yii::$app->getSession()->setFlash('error', 'Podany email jest już używany');
            }else {
                Yii::$app->getSession()->setFlash('error', 'Błąd serwera');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {

        $validation = new CookiesController();
        $validation->CookiesValidation();


        $model = new UsersForm();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionViewUser($id){

            $model = new UsersForm();

            $validation = new CookiesController();
            $validation->CookiesValidation();
            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');

            $api = new Api(Yii::$app->params['apiDomain']);
            $curl = $api->get('user/'.$id,[
                "Content-Type: application/json; charset=UTF-8",
                "Cookie: $sid",]);
            $arr = json_decode($curl->response, true);
            $model->username = $arr['name'];
            $model->email = $arr['email'];

            if($arr['type'] == 3){
                $type = 'SuperAdmin';
            }elseif ($arr['type'] == 2){
                $type = 'Admin';
            }else{
                $type = 'Prelegent';
            }

            $model->type = $type;


            return $this->render('view', [
                'model' => $model,
            ]);
    }

    /**
     * @param $id
     * @param $sid
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionDeleteUser($id, $sid)
    {

        $delete = new Api(Yii::$app->params['apiDomain']);
        $delete->delete('user/' . $id, $sid);

            Yii::$app->getSession()->setFlash('success', 'Usunięto użytkownika');
            return $this->redirect('index');
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionUpdateUser($id)
    {
        $model = new UsersForm();

        $validation = new CookiesController();
        $validation->CookiesValidation();
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');

        $api = new Api(Yii::$app->params['apiDomain']);
        $curl = $api->get('user/'.$id,[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $arr = json_decode($curl->response, true);

        $model->username = $arr['name'];
        $model->email = $arr['email'];
        $model->type = $arr['type'];
        if ($model->load(Yii::$app->request->post())) {

            $username = explode(' ',$model->username);
            $username = count($username);
            if($username == 1){
                $model->addError('username', 'Nazwa użytkownika musi zawierać imię i nazwisko');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');
            $a = [
                'name' => $model->username,
                'email' => $model->email,
                'type' => $model->type,
            ];

            $curl = $api->patch('user/'. $id, $a,$sid);

            if($curl->responseCode == 200){
                Yii::$app->getSession()->setFlash('success', 'Zaktualizowano dane użytkownika');
                return $this->redirect('index');
            }elseif($curl->responseCode == 400){
                Yii::$app->getSession()->setFlash('error', 'Złe dane');
            }else{
                Yii::$app->getSession()->setFlash('error', 'Błąd serwera');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}