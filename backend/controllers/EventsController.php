<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 12.07.2018
 * Time: 17:41
 */

namespace backend\controllers;

use backend\api\Api;
use backend\models\EventsForm;
use backend\models\SignupForm;
use backend\utils\Validators;
use Yii;
use yii\web\Controller;

class EventsController extends Controller
{

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCreate()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        //validation
        $validation = new CookiesController();
        $validation->CookiesValidation();

        $model = new EventsForm();
        $model->scenario = 'create';

        $model->lat = ' 50.854703';
        $model->lng = '16.496629';


        if ($model->load(Yii::$app->request->post())) {
            $api = new Api(Yii::$app->params['apiDomain']);

            if($_FILES['img']['error']  !=0 ){
                $result_of_base64 = '/9j/4AAQSkZJRgABAQIBTQFNAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAIAAgADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAEprKGBBHBGDSnNcVcePre08RT2U0WbKM+X5yZLBu5I7jPp6d80Noxq1oUkud2udnGgijVFzhRgZOafUNtdQ3luk9vKskTjKupyCKmoNV5C0UUUDCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAxPE+rf2LoU9yrATEeXF7uen+P4V4oSWOSSSTnOck11vxB1f7drIsYmzDafKcHq56/pge3NcjWcnqfM5lX9rW5U9I6f1+RsaF4ivdBuN0DboGOZIWPyt9PQ+9euaPrFtrWnreWxO0kqysOVYdQa8Mrr/h/q/wBi1g2MrYhu+Fz0Djp/Ue/FEZa2Ly/FypzVOT91/g/L+up6vRRRWh9GFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFACVma7qiaPo1xetjci4RT/Ex4A/OtOvL/iPrH2nUItLib93b/PJz1cjj8h/6F7UN2Ry4uv7Gk5dehxUkjyyPLIxZ2YszE8kk55pKSlrE+Tb6sWnRyPFKkkbFXQ7lYdiORim0Ugv1R7loWpprGj296uNzrh1H8LDgj86068x+HWr/AGe/l0yRv3dx88fPRx1/Mf8AoPvXp1bRd1c+swlf21JS69RaKKKZ1BRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAFS/uWs7Ce5WJpTFGXCIMliB0FeDXNxJd3MtxM26WVi7H3NfQWBXGeKvBMOpK97pqLDefeePgLKf6H3796Uk3seXmWGqVoqUOnQ8spadLFJBK0UqFJEOGVhgg02sj51pp2YtFFFIRNbXElrcxXELbZI2DqfcV7lpeoR6pplvexfdlTcR6HuPwORXhFd/8OtX2yTaVK/DfvYc+v8AEP5H86qD1sepldf2dX2b2l+Z6NRRRWp9GFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA3rVe/vIrCxnu5ziOFCzfh/WrNef/ABJ1nZbw6TE2GlxLNj+6DwPxI/Shu2pz4msqNJzZz+leONQsdUmuJyZraeQvJAT93/c9MDjHt+I9U07UbXVbNLq0lEkbj8Qe4Poa8CFbHh/xBdeH74TQkvCxAlhJ4cf0I7GoUu54mEzCdOXLUd4v715/18ux6P4t8JR63Cbm1VY79BweglHof6GvJZYnglaKVGSRG2srDkEHBBHrXvGnahbarYRXdrJvikGR6g9wfQ1ynjnwuL+BtUsk/wBKjH71AOZFHf3YD8x+FOUb6o68fg1Vj7alv1t1/wCCeY0UUVkeALVmxvJbC/hu4T+8icMOevqD7EZH0NVqWkNNppp2aPerG7ivrGG6hOY5UDL+NWDxXB/DrV98E2lSt80WZYs/3T1H4H+dd5XQndXPrsNWVakpodRRRQdAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAQzzJbW8k8rBYo0Lux7ADJrwjV9Rk1bVbi+k4MrkhfRew/KvRPiPrH2XTI9NibEt0d0mD0QH+p/QGvLR0qJPofP5tX5pqktlv/XkvzFFLSClqTyjofCfiSTQdQAkLNZTHEqD+E/3h7j/AD2x7JHIksayIwZGAKsOhB6V8916L8PfEJdTo1y+SoLW7H06lfw6j8aqL6M9bLMXyv2M9nt5f8DsZHjjw7/ZV/8AbbZcWlweQBwj9ce2eTXJ17vqumw6tps9lMPllXAP909iPcGvD7y0msb2W1nXbLCxRh7g9R7HqD6GpkrO5jmWG9lU54r3Zf1/wV8yGlpKWoPOL2k6hJpWqW97HyYnBI/vDuPyr3CCZLm3jmiYNHIodWHcHkV4FXp3w81f7VpsmnStmS2+ZM9Sh/wOfwIq4PoetlNflm6T2e39ea/I7WiiitT6AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooATFMd1jjZ3IVFGWJ6AU/Nc/4yF7J4YuksELuwAcA87P4sev/ANegzqT5IOS1seUeINVbWtbuLwk7GbbGPRB047ep9yazB0ooHSs2fHzk5ycpatu7FFLSClpCFqa3uJbW5juIW2yRsGU+9Q0tISbTutz3fR9Sj1fS4L2PAEi5K/3WHUfnXD/EfSNk0OrRLw/7qbHr2P5cfgKb8N9V8u7n0uRvlkHmxA/3gOR+X8jXc61py6rpFzZNj94hCk9mHIP5gVp8SPpP98wnn+q/r8TwulodGR2RwQykgg9QRwRRWJ809xa0tA1RtH1m3uwfkB2yD1U9fr6/UCs2jvRezuioScJKUdGndHv6ssiK6kMrDII7inYFcn4E1f7dov2SVszWhCcnqn8P9R+FdaK6E7q59fRqqrTU49RaKKKDYKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBpwBz2qOGSOaJZInV0cblZTkEeornPHOsDSvD8kcbYuLrMSY7D+I/l/MVzPw98SNBcLo905MUh/0difuN3X6H+f1ovrY454yEK6ovr18+hD498MjT5/7UtExbTN+9UDiNz3HoD/OuJHSvoC9s4dQspbSdQ0UqlWH+HvXhep6fLpepXFjN9+F9ucY3DqD+RFRJW1PIzPDeyn7SK0f4P+v1KgpaQUtSeaLS0lLSEXNMvX07U7a8TOYZAxA7juPxGR+Ne7I6zRrIhBVgCCO4Ir5+r2XwZffbfC9mWOXiBhb/AIDwP0xVU3q0exlFW0pU36/doed+NLD7B4oudowk+Jl/Hr+oasCvQviZZ/u7C9A6FomPrnkD9DXntRJas4cbT9nXlHpuvnr/AJi0d6KO9SzkNrwvqv8AZGuwzu2IXPly+m04/l1r2cYPSvn8V674L1f+09BSORsz22In9x/Cfy/ka0pvoe1lFfV0X6r9TpaKKK1PdCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBKO9Fc/wCL9Y/sXQJ5UfFxN+6hx1yR1/AZP4UyKk1CLnLZHm3jXWP7X8QS+W2YLbMUfPBx94/n39AK5+OR4pEkRirowZWHBBHIx75pgpR0rN7nyFWpKpNze7d/69D3jQNTXWNEtb4Y3SJ84HZgcMPzFcR8TNN2XNpqSLxIDFJx3HK/mM/kKt/DC9L2d9ZMT+7dZVz6NkH9R+tbXju1W68J3RxloSsq+xDAH9Cap6o9+o/rOC5nva/zX/DM8bFLSClrM+dFpaSlpCFr0j4Z3Bayv7U/wSLIPxBH/steb12vw0mK63dQ9nt935MP8acXqdmXy5cTHzujqvHsAn8KTtjJidHH54/kTXkVe2+JI/N8Naiv/Tu7fkM14lRNanRm8bVlLy/IWjvRR3rNnlCiuh8Hat/ZeuxiRsQXH7qTnueh/Pv6E1zwpaE2ndF0qjpzU49GfQHeisLwpq39r6HDK7Zni/dy56kgdfx4NbtdN77H2NOanFTjsxaKKKCwooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBv4V5B4/1j+0tea1jbMFnmMY6F/4j+gH4e9ekeJdXGh6FcXmR5uNkQPdz0/x+gNeFszOxZiSxJJJ5Jz1JpN9Dxs1r2iqS66v9P68gFKOlIKUdKg8M7X4aSEeIbiPPDWzfmGXH869C8QoH8N6mmM5tZP8A0E1558NIy3iKd8cLat/6Ev8A9evQ/ELBPDmpvnGLWT/0E1cdj6DAf7m7+f6nhQpaQUtZnz4tLSUtIQtdX8PGx4nI/vQOP1BrlK6v4ernxPn+7A5/kKI7o6MJ/vEPU9O1YbtHvV9bdx/46a8Ir3fVjt0e9PpA5/8AHTXhFVPoehnHxQ+f6C0d6KO9ZM8YUUtIKWkB1vw+vZIdda0HMVxGcj0I5B/nXqRFeS+AxnxVAfRHP6V6371vT+E+kypt4fXuxaKKKs9MKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooASiqdxqmn2f8Ax831tD/10lVf5msq48ceHbYkNqSOR2jRn/kKZnKrTh8UkvmdDRXEXHxO0iLIht7uY9jtVQfzOaybj4qTtxbaZEvvLKW/QAfzpHNLH4eP2vzPTaMivHbj4i6/OD5b28H/AFziH/sxNZVx4o126z5uq3X0R9n/AKDgUrmEs2or4U3936s3viNrP2zVk06Jsw2n38HguR/QYH4muLFK7tI5d2LMxJZjyST1JPc0gpPU8OvWdWo5vq/w6L7hRSjpSCnqpZgqgliQAB3J7CpMj0f4YWZW3v70jhmWJT9OT/MVveOrlbbwnd84aXbEvvkjP6Zq94c0v+x9CtbI48xV3SEd2PJ/wrjvibqIZ7PTUP3f37/j8q/+zfnVvRH0U19WwPK97W+b/wCHPPRS0gpazPnRaWkpaQha7T4axFtdupccJblfzYf4VxdejfDO3K2t/dEcO6xj8AT/AFFOK1OzL482Jj5anV+I5PK8N6i3/Tu4/EjFeIV6946uPJ8KXAzgysiD8wf5A15DRN6nRm8r1VHy/P8A4YWjvRR3rNnlCilpBS0gOq+Hse/xNu/uQM36gf1r1UkDrXm3w2h3apeTf3IQv5nP/stdp4mna28OX8qsVYRFQw6gnj+tbw0ifR5c1TwnO/N/d/wxrUV4rb+I9Ztv9XqVx9Hffj/vrNacHjzXIvvyQz/9dIv8MVPtETHN6L+JNfc/yZ6vRXnkHxInA/f6dG3ukhX9MH+daUPxD02TAlt7mInvhWH86tTi+p0xzDDS+1990djSGsODxfoVxgC+VD6SKV/mMVp29/Z3mfs1zDNj/nm4bFNNM6YVac/hkn8y3RRRTNAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAEopAc+lUbnWtLszi51G1hI7PMoP6mmS5JK7L1LXM3Hj7w1bZB1ESMO0UbN+oGP1rIufitpMeRBZ3kp9WCqD+p/lRZmMsVQjvJHedaXHFeV3HxZumH+i6XDH7yyl/wBABXQeBvFt14kkvIb4QrNFtZBEpAKng9SehA/OhpmdPG0ak+SLu/Q7PpTJZ4oU3yypGo7uwArD8Y29zceF7z7HNLFPEvmqYnKkgckcdeM14W8kkzbpHZ2P8TEkn86LGeLx31eSjy3PerjxVoNqD5uq2uR2SQOfyXNZVz8SfD0H+rluLj/rlER/6FivGKKdjz5ZrVfwpI9QuPitbqf9F0qV/eWUL/IGsm4+KOsSZEFraQjtlWYj9QP0rhqWg55Y/ES+1Y9F8J+OtQu9fS11a5V4bgbI8IqhH7dB0PT64r0m4gjureW3lUNFIpRl9QeDXzkCQQQSCOQQeQa9p8FeJl17TRFO4+3264kB/jHZx/X3+opHoZdi3O9Ko7vp59zy3xDoU+gatJaSgmM/NFIejqf8On+RWUK958Q+H7XxDpxtpxtkX5opQOUb19x6ivFtX0a90O+a1vYijc7GH3XHqp7j9fpSZw43ByoS5or3X+Hk/wBO/qUKBRQKRwiilFIKUUgFoFFApCFFdv8AD/w6b2+Gq3K/6Nbt+6BH33Hf6L/PFZvhbwlc69OJZQ0Ngp+eXu/sv+PQfXivYLa3gs7aO3t0EcUa7URRgAU0tbnq5fgnOSqz26ebFuriKztpbmZgkUalnY9gOteE6vqMmratcX0mQZXJCnsvYflXX/EDxKLhzo9o+Y0P+kOP4mzwv4dT7/jXLaLoN9rtz5NpH8oI3ytnYn+falJ30QZhXdeoqNPW35/8AzBW7pnhHWdUAeG0McR/5aTfIP8AEj3xXpGheDdM0VVkZBc3Q586Reh9h2/nXSdqaj3NKGU6Xqv5L/M87tPhgcA3mpDPdYY/6n/CtJPhvoyj5prxj7yKP/Za7Kinyo9COAw8VZRRxr/DfSGB2z3iH/fU/wDstb2haLDoWnfY4XaQby5ZupJ/yK08/wD6qP8AOKdkjSnhqVOXNCKTOK+IsV5PptolvbyywrIZJWRc7cDjOOg5NeZV9BYrntb8HabrG6QJ9nuTz5sQ6/UdD/OolG7ucGOy+VaXtIPXTT07Hj9HetLWNCvtEuPKu4/lJOyVfut/9f2NZvesmmtGeDOEoScZKzXRiilpBS1JJ6P8NrfbYXtyf+Wkqx/98jP/ALNWj49n8nwzIn/PaVE/Xd/SpvBVp9l8L2uRhpSZD+J4/TFYnxJucRWFoD95mkb8OB/M1s9IH0Uv3WX672/P/hzz8UUCiudnzgopRSClFACiu2+HEO69vpv7sap+ZP8AhXEivR/h1Bt0u7n7vNt/AAH+pqqa947stjzYmPlf8v8Agm34muXs/Dt5PG7JIEwrKcEEkDj8686g8Xa5AABfM6jtIit/Su08ez+V4bMf/PWZEx+bf0rzDtVVZNSsjpzSvOFdKDa07+Z10HxB1NMedBbSgeilSf1NaUPxEhb/AI+NPkT/AK5yBv5gVwHaioVWa6nJHMcTH7d/U9Sg8daLL995of8AfjJ/9BzWhF4h0i4AMeo2/PZnCn8jXj1LTVdrdHRHOKy+JJnuUcscq7o3Vx6qc041wfw7hzJfzHsEUf8AjxP9K6XxNey2GgXNxA+yVdoVvqwFbxnePMezRxPPQ9tJW0b+42KK8ug8a61D96aKb/rpGP8A2XFaMHxCuV/19jE/ujlf6GpVaBhHNcM9216r/hz0CiuRg8f6c+BLbXEZ9QFYf5/CtKHxdos+ALwIx7SIR+uMVXtIPZnRDGYee0195tjHalqlDqdjcnEF5BIfRZAau54qzojJSV0xaKKKCjI0y71Ca8vbbUbeNPKYGCSLO2WM9Dz3BBBHb8cnP8dPfQeFbi50+5lt5oGVyYzglc4P88/hXS+1V761jvrG4tJOUnjaNvoRg/zpmM4N03G+vc+dbjUr+8/4+b25m/66ys38yarCpLi3ktLqW3lXbJE5Rh7jIP61GK1PlJN3fM9fMUUtIKWkSA6V0HgzVRpHii1mZsQynyZTnHytxk+2cH8K58dKWkXCbhJTW6aZ9LkBgQwBBHINeAeJdIbRNfurLBEQbdEfVDyv1wOD7g1694N1v+3PDlvK7ZuYf3U2eu4d/wARg/jWP8StBN9pSanCmZ7TPmYHJjPX8uv0JqD3cdTWIw6qQ6a/5nklFFFUfPC0tJS0gFq1p9/c6Zex3drIY5YzkHsR3BHcHpVWgUhptNNOzR7l4Z8U2fiK2+UiK8UfvYCeR7j1H8q1NT0uy1a0NtewLLGemeqn1B7H6V8/29xNazpPbyvFKhyrocEV6NoHxKRlW31pNrdBcxrkH3ZR0/DP0pHuYbMYVI+zr6P8GUdZ+Gt7blpdKlFzF18qQ7XH0PQ/pXG3en3mnybLu1mgbt5iEZ+hI5/Cvf7S+tr+ETWlxHNGf4o2yKlZEdSGUMD2IosVVyulU96m7fij5zFKK9+k0LSJTmTS7Jz6tAp/pUkOladbndBY20R9UhVf5ClY51lEv519z/zPEdP8PatqhH2SwmdD/wAtCu1PzPFd3oXw4gt2W41eUTuORAmQgPuep+nArvh0rM1TX9N0eMte3SRvjiMHLn6KOadkdNPL6FFc9R39djQijSGJY4kVEUYVVGAB7Vw/jDxsloj6dpcge5PEsyniP2B/vfy/lgeIfH15qitb2Ia0tSCCc/vH/Ht+H5npXPaPpc+s6nDZW4+ZzktjIRe5/wA9Tgd6TfRGGJzBzfssP10v/l/n9xoeGPDNx4ivTktHaRkebL3PfaM9z/nsD7FY2Ftp1pHa2kKxQoOFUfqfemabp1vpVhFZ2qBY4x+JPcn3NXaErHdg8HHDx7ye7FooopnaJxSGsvUfEGlaVxeXsccn9wfM35DmsGX4k6SjER293J77VA/nmldI56mJo03ackmdkKWuMh+JGkuQJLe7i99gYfoc1vad4g0rVeLS9jkf+4flb8jzQmmFPE0ajtCSbNaiiimdBUvbG31G1e2uolkiccqf5/WvJPEnhufQLwYzJaSH91KR/wCOn3H6/mB7LVPUtPg1Sxls7ld0cg/EHsR71Mo3OLGYOOIj2ktmeFCpIYnnnjhjGXdgqj1JOB+tWdV02fSNSlsp/vIeGHRh2P8AnpyO1avgqw+2+JoGYZjt8zN+HT9cflWCTvY+ahRlKqqdtW7Py11/U9XtLdbSzht0+7FGEX6AV5d47vPtXiaSMH5bdFj4/M/zx+FeqyyJDC8shARFLMfQAZrwy9umvb+4un4aaQuR6ZJOPwzWtV6WPazafLSjTXV/kQiigUVzs+fFFKKQUooAUV634Nt/s/he0BGDIGkP4nj9MV5KitI6ooJZiAAO+TgCvcrO3W1sYLdfuxRqg/AYrWitWz2MnhepKfZW/H/gHFfEa4wtjbA9S0hH5AfzNcH2rpPHVz9o8SNGDxDGqfmN39f0rm+1Z1NZM4sfPnxEn20+5f8ADi9qKO1FZnGLS0lLSA9H8AQlNHnlI/1k/H0AH/16f49m8rQo4h1knUH6AE/0rQ8J2/2fwzZqRyylz/wIkj9DXPfESfMlhbg9AzkfiAP611S0pfI+kqfusvt5L8f+HOIpaSlrkPmhR1pR1pB1pR1pAFd14AVzFeuzMVBRVBPAwD/jXC16P4Ei8vQpJD/y0mJHvgAf0rWh8Z6OVR5sSvJNmj4kv5tN0SW4t22zblCkjPUjP6ZrA0LxFrerXiwCG3eNcGSQoRtH59fQVs+JtPuNVtbayt+N04LueiqAeT+laGm6bb6XZpbWyYUclj1Y+proam59kj2Z061TE3jJqCSv5v8Ardl+iiitT0DxP4k6UbDxKbtFxFeoJBxxvGA39D+NccK+hde0Gz8Q6a1pdrg9Y5APmjb1FeGa1ol5oOovZ3iYYco4+7IvqP8APB4NaJ9D53H4aVObqL4X+f8AWxnClpBS0zzgHSlpB0paQHVeAvEA0TXRHM+20usRyeit/C35kg+xJ7V7Y6JLGyOoZGBDKehHcV81V7L8P/Ew1fTBY3D5vbVcc9ZE6Bvr2P4etSz2csxP/LmT81/ked+LvDz+HtZeEA/ZZcvAx5yvcZ9Qf0we9YFe++JNBg8Q6RJaS4WUfNDJjlG9foehrwq9srjTr2W0uozHPE21lP8AMeoI5B7gg0JnJjsK6M7x+F/h5f1+hBS0lLQcAtAooFIBe9KKTvSikBPbXdzZS+ba3EsMn96Nip/nXR2fxB8QWoAe4juAO00Y/mMVy1ApGkK1Sn8Da9H/AEvwO8T4o6lj95Y2hP8AslgP502X4n6swxFaWafUMf8A2YVwwpRSuzZ47Efz/l/kb974z16+BV794k/uwAJ+o5/WsNnZ2LOxZjySeST6k02gUjnnUnP4238xRXrPw90UWGkf2hKv+kXYyMj7seePz6/lXmmj2Dapq9rZLn99IFbHUL1Y/kCfwr3qONYY1jRQqKMKB2A6U0up6mVUOaTqvp/X5fmS0UUVR7xFNNHBE8srrHGgyzMcAD3rzHxL48uLx3tdJZoLboZujyfT+6P1pPHniR7y7bSbWT/RoT++K/xv6fQfz+lcVUSl0R4OPx8nJ0qT079b+QpJYlmJJJySeSfrS0lLUHji0qkqwZSQwOQRwQfWkopAdr4c8dT2bpbaqzT23AE3V0+v94frXpkMsc8KyxOrxuMqynIIr5/rtfAviN7S7XSrmTNvMf3JP8D+n0P8/rVxk72Z7GAx7UlSqvR9etz1CiiitD3jjvHuji+0sX8S5ntR82B1Q9fy6/n61H8O9NMGlzXzjDXLbV/3V/8Ar5/IV2EkaSxNG6hkYbSD3FR2lrFZWcNrACI4kCLn0HFTyrmucf1WP1n2/l+P/DGF421AWPh2ZFOJLk+Sv0PX9Aa8lFdZ4/1MXmtLaIcx2q4Pu7cn+lcmKxqO8jwsxre0ru2y0/z/AB/IcKKBRWbOAUUopBSigDZ8K2n23xJZpjKo/mt7bef54r2EkDJPFcB8ObHMl5fsOFAhQ/q38lrqPEt6LDw9eTZw5Ty1+rcD+db01aN2fR5dH2OFdSXW7+S/4Y8o1O6+3andXWSRLKzLn0zx+QxVbtSdqXtXM3d3PnJScpOT3bv94vaijtRSELTkUuyqoJZiAAO5PSm1q+HLX7X4hsosfKJA5+i/N/TH40JXaRUIc8lHu0vxR61aW4tbSGBekSBB+AxXmvja487xJIn/ADxRU/8AZv616geBXi+p3P2zVbq5ByJJWKn2zx+mK6K792x7+bz5aMYLq/yRVpaSlrlPnRR1pR1pB1pR1pAFeseGYPI8N2aH+KPf/wB9En+teZ6Zptxqt4ttbrkn7zHoo9T/AJ5r162hW3tooB92NAo+g4row8dXI9vJqT5pVGtNvx1J6KKK6j3wooooASsbxD4ftPEWnta3I2uuTFKB80beo9vUd62aKCZRU1yy2PnTWNGvND1F7K9Ta68qw6OOzL7H9OQeRVCvoDxH4dtPEenm3uF2yLzDMB80bf1B7j+uDXhuraTd6JqMllexlJF5B/hdexX1B/8ArHBBFaJ3Pm8Zg5UJXXw9H29f61KI6UtIOlLQcQtW9O1C40q/hvbV9ssRyPcdwfqMj8aqUtIabTTTs0fQOg67ba/pcd5bnB+7JHnJRh1B/wA8isrxj4Sj8Q2gntwqahCDsboHH91v6HtXlfh3xDdeHdSFzbnfE2FliJ4kX+hHY9vocH2/StVtNasEvLOUPE4wQeGU9wR2NS0fQYevDGU3Tqb9f80fP00EttO8E8bRyodrIwwVI9aZXtfivwdbeIojPFthv1X5Zez47N/j2rx2/wBPu9MvHtLyFoZkPIPf3B6EfTig8jFYSeHl3XR/11/pFegUUCg5Be9KKTvSikAtAooFIBRSikFKKQC0CigUhHa/DS087xBLcsOIITg+hbgfpur1fFeffC2IC21Kbuzov5An+tehVSPpsujy4aPmFZPiLUv7I0K7vFI8xE2x/wC8eB/j+Fa1cL8TbkppNnbg/wCsmLH3CqR/Mim3ZG+KqOnRlNbnmJYsSzEliSSSeSfWikFLWR8iLS0lLSAWiiikIWnBirBlJBBBBHUH1ptLSGe3eH9R/tbQ7W8JG9k2yf7w4P8AjWrXEfDa4L6Vd2558uYMPYEAfzBrt84reLukfXYWp7WjGb3sIaparfx6XplxeyYxEuQPU9APxOKvGuH+JMki6bZxq2I3lJYepA4/rRJ2VwxNV0qUprdHncsr3E8k0hy8jFmPqTTBSClFcp8g2222OFFAopMQopRSCtTw9px1TW7a2IzHu3yem0dfz6fjQk27IqEHOSjHdtL7z07wtp/9neHrWJhiR18x/XLc8/y/Cud+It8AlpYKeSTM4/Qf1/Ku7rxvxJqP9p69dTq2Yw2yPB42jjj68n8a3qaRsj6HMJKhhVSj1svl1MvtS9qTtS9q5j5sXtRR2opALXZfD608zUbq7I4ijCDPqx/+t+tcbXqPgqz+yeHklYYe4YyHPp0H6DP41dFXl6HoZZT58Qn0jdmprt39h0O8uM4ZYiFP+0eB+prxyvRPH975em29oDzNIWI9Qv8A9civO6dd3kaZvU5qygvsr8X/AEhaWkpaxPKFHWrem6dcapeLbWyZY8sx6KPU/wCfam6fp9xqd4ltbJuduSeyj1P+f1r1XRtGttGshDCNztzJIRy5/wAPQVdOnzu72O/BYJ4iV38K3ffyX69vUNG0e30azEEI3MeZHI5Y+p/wrTopOMV3JWPqIQjCKjFWSHUUUUFhRRRQAUUUUAJWF4l8N2viSwMEwCTpkwzAcof8PUVu0UETgprlktD5y1PTLrR7+SyvIykqfkw9R7f561Tr3rxP4YtfElgYpAI7lBmGYDlT6H1HtXiGpabdaTfyWd5EY5ozyD0I7Eeoq0z5vGYSVCV18L2f6P8ArUq0tJS0HGLWv4f8Q3vh2+8+1bdG3EsJPyyD+hHr1HuMg5FFA4TlCSlF2a6nv+h6/Y+ILIT2cuGHEkTH5oz6EenvTta0Cw1218m+iDbfuSLw6H1B/p0rwiw1C70y7S6s53hmU8MvcdwR0IPcHivVPDnxCs9SVLbUilpd9A5P7tz7eh+tSe7h8dTrr2dZWf4P+v8Ahji/EPgjU9DLTIpurMc+dGOV/wB4dvryPcdK5gV9JZBGRyK5bW/AekauXljT7HcnnzIRgE+69D+lBliMr+1RfyZ4v3pRXU6p4A1vTiWhiF5CP4oOW/FTzn6Z+tczJE8MhjlRkdfvKwIIPvnkUmeRUpTpu01b+vu/EbQKKBSIFFKKQUopALQKKBSEeofC9gdMvl7icE/9813teb/C2U79ShI4Ijcfrn+lekVS2PqMvd8NH+uoV578UVPk6Y38IaQH6kLXoXauQ+Itmbnw0JlHNtKrn6HK/wBQfwolsPHR5sPJeX5ankopaQUtZnywtLSUtIQtFFFIQtLSUtIZ6J8M1Plak/YtGPxG6u/rkvh9Zm28OmZhg3ErOPoMD+hP411tbxWiPq8DHlw8U+wVw/xKIGnWS9/OJH5V3FeefEuYGbT4Aeiu5H5Y/kaU/hZOYNLDSv8A1qcEKUUgpRXMfKjhRQKKTEKK9F+Hml+VZzak6/NMfLj/AN0dT+fH4VwNjZyX97DaQjMkrhRx09SfYDJPsK9tsrWKys4bWEYjiQIB9K0px1uetlNDnqOo9o7er/yX5mb4o1L+y9AuJVbbK48qP13H0+gyfwrx+ut8eaoLrVUsYzmO2Hze7nH9MD865KlVleVuxjmdf2tblT0joL2pe1J2pe1ZHnC9qKO1FICa2ge6uoreP78rhB+OBXtdtAlrbRQIMJEgRfoBivN/Ath9q1w3LDKWyFs4/iPA/TP5V6Le3K2VnPdSfciQuR64rooqyuz6DKaahSdWXX9P+Dc828a3v2vxA8anKW6iMfXqf5/pXO0+aV7ieSaQ5eRi7H3Jplc8ndt9zw69T2tSU+7b/wAvwsLVqxsbjUrtLa2QvI35KPU02ysrjULtLa2QvI/HHQe9eq6FoUGiWgjTDzuMyynqx9vaqp0+f0OnBYKWJld/D/Wi8/y+4ND0S30Wz8qP55Wx5kmOWP8Ah7VrUUV2pW0R9TCEYRUYqyQtFFFBYUUUUAFFFFABRRRQAUUUUAJXPeKfC1t4lsdrYju4wfJmx09j6iuhooInCNSPLLY+cL/T7nS76SzvIjHNGcFT39CD3BHIPeq9e6+K/Ctt4ksv4Y72MfuZsf8Ajreqn9K8SvbG5028ltLuJop4m2srfpj1BHII4Iqj5rF4SWHl3j0f6P8ArUgooopnGLS0lLSA6LQvGWraEVjjlE9qOPIlJIA9j2/l7V6No/xA0bUwsc8n2Kc/wTkbSfZun54NeMUCkdlDHVqNluuz/r+ux9IK6SIGQhlIyCDUF3p9nfpsvLSG4UdBIgbH514Np2t6npTA2N7NCM52q2Vz7g5H5iuqsfidqcOBeWtvcjuVzGx+pGR+lI9OGaUZq1RW/Ff18jr7v4e+H7rJS3lt2PeGQ/yORWTN8K7M/wCo1K4T/fQP/LFT23xP0qQAXFrdwsepAVgPxz/StWLx54clAxqAQ+jxOP1xijQvlwFXX3fy/wAjmj8Kn7awD9bf/wCypy/CrBy2sZHoLbH/ALPXWr4u0Bhxqtv+LY/pTW8YeH0HOq25/wB0k/yFKyF9VwPl9/8AwTCg+GGmLgz3l3J7LtUfyz+tbNn4K8P2ZDLp6SMO8zGT9CcfpVef4geHogdt48p9I4W/qBWTdfFCzQH7Jp88p6ZlYIP0zRoO+Apa+7f7/wDM7qKKOGMJEioo6KowBT/pXkN58RNbuTiAw2q5/wCWaZJ+pb+gFeo6RqCarpVtfRY2ypkj0Pcfgcine5vh8ZSrtxh08rF6q95ax3tnNazDMcqFGHsRirFFB1NXVmeBanp82lajPZTj54mK5xww6gj68Gqleu+MfC4121Fza4F9CCFz/wAtF/un39P/AK9eSyxvDK0UqMjoSrKwIII65B6Gs2rM+VxmGeHnbo9n5dvVf8EbS0lLUnILRRRSELVzTNPm1TUYLOAfPK2M44UdST7AZP4VWjjeaVIokZ3c7VVQSST0wB1NeseD/DA0S1NxcgG+mXDdxGv90f59PTlxjc68HhZYipb7K3f6er/4J0Vnax2VpDbQjEcSBFHsBip6KK2Pq0rKyEryPxxei88TTqpyluohBHqMk/qTXp+ragml6XcXsmCsSZAPc9h+JxXiEsrzSvLI253YsxPUknJP51nVeljx83q2hGkuuv3f8EYKUUgpRWB4I4UUCremafLqmow2cP3pGwW67R3NFruyCMXKSjFXb0R2fw+0fiXVZl6/u4cj/vo/0/Ouv1jUY9K0qe8fB8tflU/xN0AqxaWkVlZxW0IxHEoVR7D1rz3x7rP2q/TTYW/dW/Mnu/8A9Yfz9q3fuRPpZcuBwtlv+r/r8DkZJXnmeWQ7ndizH1JptAormPmW23di9qXtSdqXtSEL2oo7Vf0bT21TVre0UfK7Zc+ijk//AFvfFJJt2RUYuclGOrbsvmei+DNOFjoEcjDEtwfNP06KPy5/Gqnj3UPs+lR2SH57lvmH+yvP88frXVoixoFUAKoAAHavJ/FWpf2lr07Kf3UX7pOey9f1J/DFdNRqELL0PosbJYbCeyju9P8AMxqns7Oe/u0traMvK5wB6e5PYDvSWtrPe3Mdvbxl5XOFUf19AOue1ep+HvD8GiWvZ7pwPMkx+g9BWEIc78jx8Fg5YmXaK3f6Lz/IXQNAh0S1wMPcOP3kmOp9B7VtCjFFdiVlY+qp0404qMVZIWiiimWFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAJXNeLfCcHiSz3LtjvowfKlx1/2W9R/Lr9elooM6lONSLjJaHzheWdxYXclrdRNFNGdrIw6f4g9QRwRzUFe3+LvCcHiK03x7Y7+Jf3UhH3v9lvb+Rrxa6tZ7K6ktrmNopozhkbt/n171R81isJLDy7p7P9PX/hyKlpKWg5BaBRQKQC96UUnelFIBaBRQKQCilFIKUUgFoFFApCFFd38O9fFrdNpNw2Ip23Qk/wv3H48fl71wgp6uyMrIxVgQQRwRjpg+tF7M1oVnRqKcf+HXVH0TmjNct4P8UJrlkILhgL+EYcdPMH94f1/wD1V1GfcVaPrKVWNWKnB6C9q53xB4TsNfUyMPIugOJkHJ+o7j/Oa6KigdSnGpHlmro8a1LwTrWnMStsbqLPDwfMT9R1/p71gyRSQttljeNh1VgQf1r6CBz/APrpGRWHIB+tS4HlVMog9YSt+J8/xxSTNtijeRj0VQSf0rf03wVrWoMC1ubWLu8/ykfQdf6e9ewqiqOAB9KUnFLkCnlEE7zlf8DA0DwrY6CBIoM10RgzOOR9B2H6+9dD2pKKvoerTpxpx5YKyEzRmiuZ8W+Jk0S08iBgb6VfkH9wf3j/AE//AF0NpLUmrVjSg5zeiOb8fa79qul0uBwYoTumI7v2H4f19q4qlLM7MzElmJJJ6knqSaSuaTbd2fJ4is61Rzl/wy6IBSikFKKkxHCvTvA+hf2fYG/nXFxcr8oP8CdvxPX8q5Xwd4fOsaj586H7HAcvno7dQv8Aj/8AXr1clVUk4CitacftM9rKsL/y/n8v1ZleIdYTRdJluMgyt8kS+rHp+A61467tLI0jsWdyWZj1JPJz+NbXirXDrWqkxsfssPyRD19W/HH5CsMVFSV3psjjzDE+3qWi/dW36/8AAFFFAorM4Be1L2pO1L2pCF7V6D4A0oxW02pyLzL+7j/3R1/X+VcPp9lJqN/BaRD55WC5xnHqT+Ga9ntbaO0tYraJcRxKFUewrWjG7uz18pw/PUdVrRber/yX5md4k1P+ytEnnVsSsPLi9dx4/Tr+FeT21tNeXKW8EbSSucKo/wA8DuT2rp/GN7Lq+ux6ZaK0nk/KFXu56/kP611Xhvw5FolvvcLJeOP3kmOnsPb+dOSdSVuiNa9OeNxPLF+5HRvz62/Id4c8OxaJb7mxJdOP3kmP0HtW7RRW6VtEezTpxpxUIKyQtFFFM0CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooASuV8YeEYfENt50AWPUIx8j9A4/utXVUUGdSnGpHkmro+b7i3mtLh4LiNo5YztZW6g1HXtHjHwfFr9ubm2Cx6hGPlY8CQf3T/jXjc0EttO8E8bRyodrIw5U+9M+ZxWFlh5Weqez/AK6jKBRQKDlF70opO9KKQC0CigUgFFKKQUopALQKKBSEKKUdKQUo6UgJrW6ns7mO4tpGjmjOVde3/wBb1HevWfDHjO21pFt7orBfYxt/hkPqpPf2ryGlBIOQTnrkdqabR04bFzw8rx1X9fcz6IHIpa8m0P4gX+nqsN+pvIBwGLfvAPr3/Hn3r0nSdYtdZtBc2pk2dxJGVP68H8KpNM+hw+MpV17r17GjRRRTOsKKKKAG0E9eKxtS8U6RpYInu0aQf8so/mbPvjp+NcJrfj2+1ANBYg2luf4gcyMPcjp+FJySOOvjaNHRu77I6rxN4xt9HVra1Kz3xGNuflj92Pr7V5bcXM13cvcXEjSSucs57/59O1R5JJJJ9STSd6xlK589isXPESvLRf197FFLSClqDlAVe0rTbjVtQjs7dcs/JOOEHqf8+g6mqkMMtxOkMKM8shCqq8kknAxXr3hfw9HoVj82Gu5OZXHb0Uew/WqjFyfkdmCwksRPX4Vu/wBPmaOmadBpVhFZ24xGg5J6se5PvXK+OvEH2eE6VbP++kX9+w/hX0/H+Vb3iTXYtC00ynDTv8sKep9foO9eQTzSXM7zzOXkc7mY9z1q5ysuVHp5jio0oKhT3f4IYKUUgpRWB8+KKKBRSAXtS9qTtWhomlyaxqkVomQp+aRh/Cvc/wBB9aSTbshwjKclGKu27I7LwDo/lW76pKvzSApF7L3P4nj8K6fV75rDT3khXfcOfLhQD7znp/j9BVuCGO3gjhhULHGoVVHYDpQ0SNKshUFlztJ7Z612Rjyqx9dRoeyoqnDTz8+5ieHPDyaTEbi4Ilv5smSQ84z2H+Pet/NGBij1qkklZGtOnGlFQgtB1FFFBoFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXJeLPBcHiELcwOsF8uB5hGQ4HZv6GusooM6lONSLhNXR4RqnhHW9JJM9k8kY/wCWsI3r9eOQPqKwx3r6SrM1Dw7o+qZa8sIJHPVwu1j/AMCHNM8mrlPWnL7zwHvSivVr34X6ZLlrS7uLdj0DYdR/I/rWDdfC/VYiTbXVrOvbcWQn9CP1pHDPL8RD7N/Rr/gM4igV0U3gbxHDknTmcDvHIrZ/DOf0qi/hzW4vvaTe/hAx/kDSOeVCrHeL+5/5GYKUVd/sfU166beD6wN/hSrouqt93TL0/SB/8KRPJL+V/c/8ilQK1ovC+uzfd0m7H+/GV/nitC38A+IZiN1okIPeSVf6Zosyo4erLaL+5/8AAOaFKOld5a/C+8bm71CGL2iQv+pxXQWXw70S1w1x5123pI+1fyGP1JoszphluInurLzZ5NHFJNII4kaR24CqCSfoBya6bS/AWtagVaaIWcR/im6/98jnP1x9a9XstOstPQpaWkMAPXy0AzVl2WNSzsAo5JJxRyrqd9LKYLWo7+W3/BOX0jwFpOm7ZJ1N5MP4pR8oPsv+Oa6gAKoCjAHQCuZ1Tx1o+nArFL9rmH8MHI/Fun5ZNcHrHjfVtV3RpJ9kgP8AyzhOCfq3X8sZp3SNpYrC4VcsN+y/zPQ9b8X6ZooaNpPPuR/yxiIJH+8ei1wV34+1ye5aSGZLeI9IkjVsD3JGSfxrls55pahyZ5NbMa1V6PlXl/n/AEjoX8beIXBH9oFc/wB2JB/Ss261nU70EXN/cSKeqmQ7fyzj9Ko0VDbfU5ZV6slaUn9//BFpaSlpGYtHeijvSYhRTlVnYKoJZjgADqT0ApFUswVQSScAAck+1el+EPCAsFTUNQTN0eY4z0i9z7/ypxjc6MNhp4ifLHbq+iX+fZE/g/wqNJhF7eIDeuOFP/LIen1Pf/Oeh1HULfSrGS7uX2xoOg6sewHvUl3dwWVtJcXEixxRjLMT0FeSeJPEM2vXu7lLWPiKM/8AoR9z+nT3OragrI92vWp4GkoQWvT/ADZV1fVrjWdQkupzjPCJnIRfSqFAornbbd2fNylKcnKTu3q2KKUUgpRSJFFFAopALXqng7Q/7J03zp1xdXGGYH+Few+vc/WuU8F6EdRvvts6f6NbkEA/xv2H9f8AJr1AYrelD7TPcyrC/wDL+Xy/zFooorY9wKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAE7VXuZGht3lSJpSq52KQCfpkgZqwetZ2u3H2TQb+4BwY7d2B98cUyZu0W+xk2fj7w9d4BvDAx/hmQrj8eR+tbttqVjeDNreW8//XOQN/I18796cOtI8KGbVF8UU/v/AOCfSAxRXz1Dquo2wxBf3UQ9EmYf1q9H4r16IYXVbk/7z5/nmi50RzeD3i19x7xRXhw8a+Ix/wAxSQ/VFP8ASg+M/ETddUl/BVH8hSuV/a1Ls/w/zPcAMUhx6ivCJfE2uS/f1a8/4DKV/liqU15dXP8Ar7maXPXzJC38zRciWbw+zB/ej3W61zSrLIuNRtoyOqmQZ/LrWFefEPQ7bIhaa6b/AKZx4H5tj9K8gFKOlLmOaebVX8MUvx/yO5vviXfSgpY2kNup43Od7fUdAPyrlr/WdR1Rs3t5LMM52scKPwGAPwFUKB0qW2zhq4mrV+OTaFFLSClpGItLSUtIQtFFFIQtLSUtIYtPiiknlWKJGkkc7VVQSST0xirGnabdardrbWcRkkPXHAUepr1Tw54VtdBiEjYmvSPmmI4Gey+g/nTUWzrwuDniJdo9X/X9dyl4V8HJpYS9vlWS9PKr1WL6ep966i6uobK2ee4kWOJBlmY4Apl7f2ul2bXN1II4kHJ9fYepryfxH4mudeuMcxWaHMcWevu3qf5fmTo2oKyPZq1qWBpKEd+36sf4m8TTa7dbEzHZxn92nTd/tNWAKQUorBtt3Z87Vqzqz556scKKBRUszFFKKQUooAUVd0rTJ9X1GO0gHLfebGdi9z/nviqkUTzSpFGpeR2Cqq9SScAAV614Y0BNEsAHAa6l+aV/6D2H86qEOZ+R2YLCvEVLP4Vv/l8/yNOxsYdOs4rS3XEcYwP8at0UV1H1aSSshaKKKBhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAlcv8QLn7P4PulzhpmSMf99An9Aa6ivP/AIqXO3TLC2/56TNJ/wB8jH/s1By4yXLQk/L89Dy3vSik70ooPlRaBRQKQCilFIKcoJIABJ6ADvSA1/DeiSa9rEVquREPnmYfwp/ieB+Ndfr/AMOOWn0VvrbSN/6C3+P510fg3QBoWjr5qgXlxh5s9R6L+H+NdEfrVJHvYbLoextVWr/A+fLq0uLGdoLqCSGUdUcYP156j3qEdK9+1DSrHVIfJvraOdO24cr9D1H4Vw2r/DXlpNJusf8ATGc/yYf1FS0zjr5XUhrT1X4nndA6Vf1DRNS0o4vbOWJf75XKZ+vIqgOlSzzZRlF2kmn56CilpBS0hC0tJS0hC0UVasNOu9TuBBZwPLJ3x0X6k9KW41FyaSV2+xWro/D3hC91tlmcG3s+vmsOWHoo/r0rrPD/AICtbLbcamVuZ+ojA/dqf/Zvx4rswAoAHAAq4w7nsYXK27Srbdv+D/l95R0vSLPSLUQWcIRf4ieWY+pNQa3r9loNt5lw26Vv9XCp+Z/8B71ieIvHNvp++103ZcXI4MmcpGf/AGY+351wEdvquv3rSJHNdzufmfHA+p6AenQCnKVtEdGIx0aX7qgry8tl/X/DsfrOuXmuXZmunwoz5cQPyoP6n6/4Cq9hp15qdwILOB5X77Rwv1J6V2mkfDs5WXVp+OvkQn+bf4fnXcWVha2FuILS3SGMfwoMZ+vrUKDbuzkpZbWrS56ztffu/wDL+tDjbX4eRJpcwupt186HYynCRnt7n61wE0MltPJBMhSSNtrKexr3rHGK4Hx/oOcaxbr6LOB+Qb+n5U5wVrroa4/AQjSU6Sty7+n/AADgRRQKKwZ4QopRSCu48G+FfOKapfpiMfNBE38X+0fb09fpTjFt2Rth6Eq8+SH3+X9feaHg3wz9ijXUr2P/AEh1zEjf8swe59z+n411Go6hb6XYyXVw22NB0HUnsB7mrEssdvA8srqkaLuZj0ArybxN4hk1y9whK2cRxEnc/wC0ff8Az653k1BaH0FWpTwFFRhu/wAe7f8AXkei+HNVfWdIW7kAVzI6lR2GTgflitfFcV8Op92n3dv/AHJQ/wD30Mf+y121VB3jc6cJUdWhGb3sLRRRVHSFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFADT0ryj4o3O/W7S2zkRW+7HoWY5/9BFesd68P8dXP2nxhfEHKxlYx7YUZ/XNM83NJWoW7v/gnO96UUnelFI+dFoFFApAKK7X4feH/AO0NQOpXC/6NasNmf4pOv6cH8RXFCtfRfEeo6FKTaTfuictC/KN+HY/SkbYacIVVKpsv6R7xSd65bQvHWmauFimb7HdH/lnK3Df7rd/0PtXU9qs+pp1YVI80HdDqKKKRqMZQ6FWAIPUEcGsG/wDBuhahlpLJYnP8cHyfoOD+IroKKLXM504VFaauvM88u/hgpybLUWA7JNHn9Qf6Vh3Pw+16D/VxwXH/AFzlx/6FivX6MUnFHHPLMPLZW9Dw6bwxrkH39LuT/uJu/wDQc1Cmh6rJMsS6bd726AwsPzJAA+pr3bGKXGankRz/ANj0/wCZ/gec6L8OnYrNq0u1evkRHJP1b/D867uz0+0063EFpBHDEP4VH8/WrWOKWrSS2O+hhaVBWgvn1EbOM+1c3qmk6xre6GW/isrPoY4AXZx/tMcf59a6Q8UdqLGlSmqitJuxzOn+BtFssPJC9045zM2R+QwP0roooYoIhHFGkcY4CquAPyqXFIaEkFOjTpq0EkOooooNRo9aZNDHcQPFKgaN1Ksp6EHrTtwVckjA6muQ1zx1aWIaDT9t1cdC/wDyzX8e/wCH50m0tzGtWp0o81R2Rw2vaPJouqyWrZMZ+aJv7y//AFqzKs32oXWpXLXF3M0sh7noB6AdAPYV1PhTwgb0pf6im21HzRxEcy+59v5/TrzW5pWR8tGj7eq40Vp59F5/15asb4R8JnUGTUL9CLUHMcZHMvuf9n+delEqi8kKqj6AUALGoAACgfQCvOfF3iw3pfTtPc/ZhxLKp/1nsP8AZ/n9Ou+lOJ7qVLL6Pdv8X/l+RB4t8UHVJWsrNyLJD8zD/lqR3+mfzrlaBRXNJuTuz52vWlWnzz/4byXkdj8PJ9mrXNvniSHd9Sp/+vXpGMV5J4On8jxPZ5OFcsh98g/1r1vPFb0X7p9BlM74e3ZsdRRRWp6gUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAJXjmveDfEL6peXgsvOSWZ5AYnB4LEgY4Newu6opLEAAck0DBGR3pnNicNDERUZPY+ebnTb6zP8ApNncQ/8AXSNl/pVYV9IYyOapz6Ppl1zcafayn1eFT/MUjzZZR/LP70fPlAr3KXwb4fmPz6VAP9zK/wAjVV/h/wCG26WTp/uzP/U0WMXlVbujxgUor2I/Drw//wA8Zx/22NKPh54fH/LvMfrM1KzF/ZVfy/r5Hjtb+jeMNX0YCOOfzrcceVN8wH0PUfTp7V6SngLw4mD/AGeWPvM5/rVyLwpoUA+XSrU/76bv50WNaeW4iD5ozt6XMnSfiFpV/tjut1nMeMPyhP8AvD+uK6xWSSMOjBlPIIOQait7K1tRi3tYYv8ArnGFqwcYpnr0o1IxtUab9LBRTJZY4ULyuqKOrMcCsW58X6DauEk1OFj/ANM8v/6CDQVOpCCvJpG7xRWbaa7pd9j7NqFtIT/CJBn8utaPamVGUZK8XcdRRRSKCiiigApKKgnu7e1TdcTxxL6yOFH60CbsTnmkPSufvfGuh2QI+1+e4/hgXdn8en61zV/8SZmyun2SoOm+Zsn8h/jUuSRy1MdQp7y18tT0RnVFLMQFHUk4rmNW8c6Xp4ZLdjdzDtEfl/Fun5ZrzbUNa1LVWP2y7kkX+5nC/kOPxqjUOp2PLr5tJ3VJW83/AJG1rHijUtaJSaXy4O0MeQv49z+P5VjgHOMcnoBT7a2nu50gt4mllfhVUf5/OvTfDPgyHS9t1fBZrzqF6rF9PU+9Qk5M46NCtjJ3bfq/6/BfgZnhbwUSUvtWj4+9HbEfq3+H513zMsaFmIVVGSTximXFxFaQPNPIscSDLOx4ArzHxR4uk1dmtbMtHYg4OeGl+voPb8T6DVuMEezKVHAUrLd/e3/kWfFfi43xew05yLbpJKOsvsPb+f068eKQUornlJt3Z8/XrzrTc57/AIJdkKKKBRUmJa06f7LqVrOekcysfoDzXt/avB+1e3aZP9q0q0uDyZIVc/iK2ovdHt5NP44ej/QuUUUVue6FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHMePbn7N4OvcHDSbYx75YZ/TNeO2mqX9jj7Le3EA9I5CB+I6GvS/inc7NGsrbvLPv/BQf6sK8pFM+dzOo/rFk9kv1Z01t498RW5AN6JVHaWNT+owa1oPijqi/wCvsrSQf7O5P6muE70opHLHF147Sf5/melRfFVSR5ukEepWfP8ANRVpPilpx+/YXQ/3Sp/qK8soFK5ssyxC6p/I9ZHxQ0fHNpff98J/8VSH4oaRjizvj9VQf+zV5QKUUXY/7TxHdfd/wT1CT4o2Q/1enXDf7zqP8aqS/FJyD5WkqPQtPn9Nv9a87oFK7IeZYl/a/BHaz/EvWZARFBaQj12Mx/MnH6VlXPjLxBdDD6lKg9IgEx+IGf1rAFKOlJtmMsVXl8Un99vysTT3M90++eeSVvWRix/U1EOlFA6UmYNt6sUVZgv7y1GLe7nhH/TOQr/UVWFLSBNrVOxsxeK9eh+7qlwf99g38watDxx4iHW/z9YU/wAK52lou+5osRVW0n950h8deIMcXij/ALYp/hUT+M/ED8HUnA/2Y1H8lrBopcz7jeJrdZv7zSm1/V7jiXU7th/d81gD+AOKoszSMWdixPUk8n8aZS1N2zKU5S1k2/Vt/mxaO9FPiikmlWOJGeRjhVUZJ/KglJt2Q0Vr6J4dvtdnC26bIAcPO4O1f8T9K6bw/wCAGbbcawdo6i3Q8/8AAj/h+favQIYYraFYoY1jjUYVUGABVRpvdnrYXLJTtKrou3V/5fmZei+HrLQrfZbJulYYeZvvN/gParGqavZ6Pame7k2r0VRyzn0A71keIfGFpo4eCArcXg42A/Kh/wBo/wBP5V5lf6jdapdNc3kzSSHgeij0FVKairI7MTjqeGj7OirtdO39f8OaGveJLvXZ/n/dWyn5IVPH1Pqf5du9Y1AorBtt3Z8/UqTqS5pu7YopRSClFIgUUUCikAvavWvB0/n+F7XJ+ZNyH8CcfpivJe1ekfDyRjo9zGQdqz5BPQ5UcD8q0ov3rHpZTK2It3T/AEOyooorpPpgooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA8p+Klzv1extf+ecBf8AFjj/ANlFcCK6Xx9c/aPF92M5EQSMf98g/wAya5oUz5TGS5q8mL3pRSd6UUjmFoFFApAKKUUgpRSAWgUUCkIUUo6UgpR0pALQOlFA6UgFFLSCloAWlpKWkIWiilAJOADzxgd6QBS966TSPBGraptklQWkBx88w+b8F/xxXf6P4Q0vR8SJF59yP+W0uCQfYdB/OmotnbQy+tVs2uVef9f5HBaJ4K1LVSssym1tjg75B8x+g/xx+Nej6P4e07RIsWsOZCMNM/Lt+P8AQVps6xoXdgqgZJY4xXH654+tLMNBpoW6m6bzny1P17/h+daJKOrPXhQw2Cjzy37vf5I6m9vrXT7Zp7udIo16s3f/ABrzrX/HVxfbrbTQ1vB0Mn/LRvp/dH6+4rmdQ1O81W4868naV+wJ4X6DtVSs5VG9EebisznU92n7q/F/1/TDJJznk9zSikFKKyPMHCigUUmIUUopBSigBRRQK6Hw14Xm1uYTTbo7JT8z9C/sP8/rQk5OyNKVKdWfJBXb/rXyI/DnhyfXLncd0doh/eS46/7K+/8AL8gfVbS0gsrZLe3jWOJBhVFFrbQ2dslvbxrHEgwqqOBWXN4jtk1630qM+ZK7ESMDwhAJA+vArpjFQPpMNh6WDh7z959fPsjdoooqz0AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigApKKq6jcG0026uAOYomcfgCaBN2VzwTWrj7Zrl/cA5Elw7L9M8fpiqIpTnJz+tIKZ8bJtybfW7+8XvSik70opCFoFFApAKKUUgpRSAWgUUCkIUUo6UgpR0pALQOlFPjjeVgkaM7Hoqgkn8BQxW7DRS1tWXhHXb3Bj06VF/vTYTH/fWD+ldHY/DK4bDX99HGO6QruP5nH8jQk2dNPB16nwxfzODq7Y6Vf6m+yytJZucZVflH1J4H4mvV9P8EaHYbW+y/aHH8Vwd36fd/SugRFijCIqqo4AUAAU1Dud9LKJPWpL5LX8zzbTPhvdSlW1K5SFepji+Zj7Z6D9a7XTPDml6OoNpaIJQP9a/zOfx7fhWsOa5LxP4tu9DbyodMkJbhZ5v9Wfpjr9Mg1VlE71Qw2Ejztbdd2dXJIkaFnZVUDJYnAFcpq/j7TbHdFZ/6ZMOMocID9e/4V51qeu6lq75vLp3TtGPlQfgP6/nWfUOfY8+vm0npSVvPr9xrat4i1LWmP2qciLtDHwg/Dv+JrK70Ud6zbb3PJnOU5c0m2+7FFLSClqSQFKKQUopAOFFAopMQopRSxRvNIscaM7sQFVQSST0xjqa9D8NeCUttl5qqq8vVIDyqf73qf0pxi5HRh8NUxEuWG3V9F/XbcyvDHg+TUCl7qCtHadVjPBl9PoP1P616RFEkESxxIEjQYVVGAB7Cld1iQuzBVUZJJwAK898TeM2ud9lpjlYuVecdX9l9vf/ACd/dpo979xl9Pzf3t/18i94p8Xrb77DTHBm+7JOvIT2Hv8Ay+vTitLuPs+sWlwzH5J1ZjnqM85/DNUu1LXPKbbueDXxdStUVSXR6Lov6tue80lVrC5+1afb3H/PWNX/ADGas12H16d1cWiiigYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABSUVyHj3XrzQ9NtWsZRHPLNgkqGyoU56++2gzq1FSg5y2R0V3pWnX//AB92NtOfWSME/rWLc+APDlxkiyMLHvFKw/TJH6Vx1p8UdUiwLqztpwO65Q/1ratvinpzgfabC6iP/TMq4/XB/SmcP1vB1fit81/wP1Fn+FmmtzBf3Uf++FYfoBWdL8Kp1/1Oqxt7PCV/qa6aD4heHJhzePET2kib+YBFaEXirQZhlNWtB/vyhf50g+r4Ge1vk/8Agnnsnww1lSdlzZMPd2H/ALLUDfDfX16C2b6S/wCIFeqxarYTf6u+tn/3ZVNWVljf7jq30NFg/s3DS2v9/wDw547/AMK88Qj/AJd4f+/wpw+HniHvBCP+2w4r2PNGaVkL+yqHd/eeQp8N9dY8taL9ZT/QGrMXww1U/wCtvLNf90s38wK9Qa4gT78qL9WFVZNZ0uH/AFmo2if70yj+tFg/s3Cx3/P/AIY4iH4WHrNqo9wkH9Sa0YPhppEeDLcXcp7jcFB/If1ral8XaBAMvqluf9w7/wCWazrj4iaBF9yS4n/65xEf+hYo0H7HA097fN3/AFLtv4N8P2uCmnRuR3lJf+ZxWvbWttbJttoIol9I0Cj9K4l/iSJ5hDp+kzzyPwis+CT7BQa39LPiC9xLqLW9jH1EMK7pD9ScgflTTXQ1o1aDdqK+5frodBRRRQdoUUUUAFRTwRXMTRTRrJGwwyOMg/hUtFAjgtb+HkUu6fSZBC/XyJCSp+h7f56VwN7p93ptwYLyB4ZB2YcH6HofwJFe84/Kqt9p9pqUBgvLdJo/Rh09x3BqHBPY8zE5ZTqe9T91/geEUd67nW/h9NCWm0lzKnXyHOGH0Pf/AD1riZYZLeZopo3jkXgo4wR+fSs5Jrc8Ovh6lF2mvn0fzGilpBS1BgApRSCnKCSAAc5wAO9IBRV/StHvdZuRDaRFum52+6n1NdD4f8DXF6VuNS3W8HBEXSR/r/dH6+1eiWdnb2NstvawpFEo4VRWkabe56eEyydS0qmke3V/8AytB8L2ehxh1AmuiPmmYcj2A7CtS+vrbTrVri7lWONe57+wHc1l674ns9DiKO3nXRHywof1PoP8ivMNV1i81m5867kJwTsRfuoPQD/J96uU1HRHoYjGUsJH2dJarp29TU8ReK7nWnaGLdDZdkHV/wDex/L+dc9QKK55Nt3Z8/Vqzqy55u7f9adkL2pe1J2pe1SZHrnhGf7R4YsmJ5VSh/4CxFbhrj/h7Pv0a4hJ5jmzj2IH9Qa7CuyDvFM+wwc+ehB+SFoooqjpCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooASvLfitdbtR061H/LOJpD/AMCIA/8AQTXqRrxT4h3P2jxhcL1EKJGPy3f+zUzzszlbDtd3Y5egUUCg+cF70opO9KKQC0CigUgHBiBjJ/ClyT1J/E00UopALQKK6DQ/B+qa3tkWP7PankzSjAI9h3/l70iqdOVR8sFdmCASQACSeAAOtddoXgHUdS2zXubO3POGX94w9l7fj+Vd7ofhHS9DVXij865H/LeUAsPp2H4VvjiqS7ns4fKkverP5GZpGgadokWyygCsR80jcu31NamaTpS9qo9eMYwXLFWQtFFFIoKKKKACiiigAooooATNZmq6Fp+sxbbyAMwHyyLwy/Q1p9qTr6GixMoKa5ZK6PK9a8CX+nFpbLN5bjnCj94v1Hf8PyrlCCCQQQQcYPavfz0rJvfDulahdrcXNlG8qnO7kbvqB1/Gs5U09jyMRlMW70nby/rU8s0fw7qOtOPs0JEPQzPkKPX616ToXhHT9FCy4+0XQ/5bOOn+6O3863UjWKMJGiqqjACjAH0FYOt+L9P0bdEG+0XQ/wCWUZ6f7x7fzpqMYq5tSwlDCL2lR693+huzTxW8TSzSLHGoyzMcACuC8QePC+620jheQbgjB/4CP6n/AOvXL6vr9/rcu66lxGDlYV4Vf8fxrMqJVOiOHFZpKd40tF36v/L8xzO8js7szuxyWJyT+fU0CkFKKxZ5DfViiigUUgF7Uvak7UvakI7X4dThb29tz/HGrj8Cf/ihXodeU+CZ/J8TQLniVWQ/ln/2WvVq6aXwn0+VT5sOl2bX9feLRRRWp6QUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFADT0r5+1+5+1+IdQnzkPcPtPsDgfoBXvV9cC00+4uW6RRNIfwGa+dCSzEkkknOT3po8bN5aRj6sKBRQKDxBe9KKTvSikAtAoq3p+mXuqXHkWVs8z8Z2jhfqT0/OkOMW2kldsqitbRvD2p65LtsrclM4aZ+EX6nv+Ga7vQfhvbW+yfV3FxL1EKHEY+p6t+ldzFDHBEsUMaxxqMKqLgD6Cix6mHyuUveq6LstzldC8Babpe2a7xeXQ7uPkX6L/AI/pXXdBjFFBqj2qVKFKPLBWQ6iiikahRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA0dBXL634J0/U901ti0uD1KD5W+o/wAP1rqB9aWhpPRmVWjCrHlmro8U1XQdQ0aQrdwHZnCypyp/HtWbXvMkSTRtHKiujDDKwyDXG614Bt7jdNpji3l6mJjlD9D1FYypdjxMTlUo+9R18nuecilFWL3TrvTZ/Ju4HiftnofoR1quKxas9TyJRcW1JWa7iiigUUhC9qXtSAFiAASScAAck13HhvwU0my71VCqcFLc8E/73+H/AOqnGLlojahh515csPm+iKPhDQLy7v4NROYbaF9wYjmT2X27Zr0/pTFRY4wiKFUDAA4AFClXUMpBUjIIPUV1Qioqx9RhcNHDQ5I6vqSUUUVR1BRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAc941ufsvhDUXB5aPy/8Avohf5GvC6+htU0q01ixazvY2khJBwHKnI9xXIXXws02TJtb25gb0cK4H8v50zycfhKtaalBdO9up5TQK725+FmpIc21/aygf89AyH8gDWTP4A8RQE4sllA7xyr/Uig8qWErx3icz3qa2tp7udYLeJ5ZW6IgyTXYaH8ONRvnEmpE2cAPKdZG/oP8APFek6ToWnaLD5VjbrHkfM/V2+ppG+Hy2pUs56L8f69ThdB+Gssm2fWpPLXqLeJhk/wC8w4H4fnXoVnp9rptsLezt0hiH8KD+frVo8HtRTPcoYanQVoL59R1FFFI6AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAKt3ZW19bmG6gSWM/wsK4bWvADpum0l9y9fIkYA/gTx+f516DQehpSinuc1fC0q6tNa9+p4TPbzWszQzxvHIvVHBBqaw0+61K5FvaQtJIeuOi+5PQfjXsOpaPY6tB5d5AHx91+jL9DTtO0q00q1EFnEETuf4mPqT3rH2Ou+h5Syd+0s5e7+Pp/wfwMfw/4RttIVZ7jbPedd+PlT/dH9TXQT3ENpA888ixxIMszHAFZ+s69Z6Jb7rl90p+5Ev3m/wAPrXmOs6/e63NunfZCDlIVJ2r/AIn3NVKUYKyOutiaOCh7Omte36s1/EfjCXUt1rYlorToz9Gk/wAB7f8A6q7PwrP9o8M2L/3U2f8AfJK/0ryLtXpXgCcyaHLFnmKYjHsQCP1zUUpNydziy/Ezq4lubvdfJbbHXUUUV0H0AUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHMeIvGNn4cvoLa6t5pfNj35ixwM4HBx6GmWnxA8O3OA140LHtNEw/UZH61zHivQdU8TeMpI7SAiGCNIjPJkIvG7r3Pzdq6PQfAWl6PtmuALy7HO+RRtU/7K9Pzz+FM82NTFTrSUEuVdX+nc6mCeK6gSaFw8bjKsvQipaTpS0j0QooooGFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAYus+HLDWV3Tx7JwOJo+G/H1/GvPtY8JajpO6QJ9otx/y1jHI/3h2/WvWaMVEqakcOJwFKvq9H3/z7nhPau3+HU4FxfW5/iVXA+hIP8xW5rHg7T9T3Swr9muT/ABxj5Sfcf4Vh+HdIv9B8TolzFmKWN4xKnKN369vu96xjTlCafQ8ujhKuGxMG9Y9159/6+Z6DRRRXSfRBRRRQAUUUUAFFFFABRRRQAUUUUAf/2Q==';
            }else {
                $data = file_get_contents($_FILES['img']['tmp_name']);
                $result_of_base64 = base64_encode($data);

                $validators = new Validators();
                $validate = $validators->eventValidator($_FILES['img']['type']);
                if ($validate == true) {
                } else if ($validate == false) {
                    Yii::$app->getSession()->setFlash('error', 'Zły format zdjęcia! Obsługiwane formaty to jpg, jpeg, png oraz gif');
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Coś poszło nie tak z weryfikacją zdjęcia');
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }



            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');

            $split = explode(',', $model->fullLocation);

            $lenght = count($split);

            if ($lenght == 3) {
                $city = $split[1];
                $place = $split[0];
            } elseif ($lenght >= 4) {
                $city = $split[2];
                $place = $split[0];
            }else{
                Yii::$app->getSession()->setFlash('error', 'Błędna lokalizacja');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            date_default_timezone_set('Europe/Warsaw');
            $start = strtotime($model->start);
            $end = strtotime($model->end);
            if($end == $start || $end < $start )
            {
                Yii::$app->getSession()->setFlash('error', 'Data zakończenia nie może poprzedzać daty rozpoczęcia');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


            if($end > strtotime('+3 day', $start)){
                Yii::$app->getSession()->setFlash('error', 'Czas trwania wydarzenia nie może być dłuższy od 3 dni');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            $array = [
                'name' => $model->name,
                'img' => $result_of_base64,
                'location' => [
                    'city' => $city,
                    'place' => $place,
                    'lat' => $model->lat,
                    'lng' => $model->lng,
                ],
                'date' => [
                    'start' => $start,
                    'end' => $end,
                ],
                'info' => $model->info,
            ];



            $curl = $api->post('event', $array, $sid);

            if ($curl->responseCode == 201) {
                Yii::$app->getSession()->setFlash('success', 'Dodano wydarzenie');
                return $this->redirect('index');

            }elseif($curl->responseCode == 400){
                Yii::$app->getSession()->setFlash('error', 'Złe dane');
            }else{
                Yii::$app->getSession()->setFlash('error', 'Błąd serwera');
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $validation = new CookiesController();
        $validation->CookiesValidation();

        $model = new SignupForm();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @param $sid
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionDeleteEvent($id, $sid)
    {
        $delete = new Api(Yii::$app->params['apiDomain']);
        $delete->delete('event/' . $id, $sid);

            Yii::$app->getSession()->setFlash('success', 'Usunięto wydarzenie');
            return $this->redirect('index');
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\base\Exception
     * @internal param $sid
     */
    public function actionUpdateEvent($id)
    {

        $model = new EventsForm();
        $validation = new CookiesController();
        $validation->CookiesValidation();
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');


        $api = new Api(Yii::$app->params['apiDomain']);
        $curl = $api->get('event/'.$id,[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $json = $curl->response;
        $array = json_decode($json);
        $arr = json_decode(json_encode($array),true);
        date_default_timezone_set('Europe/Warsaw');


        $date = $arr['date'];
        $location = $arr['location'];

        $model->name = $arr['name'];
        $model->info = $arr['info'];

        $start = date('Y-m-d H:i:s', $date['start']);
        $end = date('Y-m-d H:i:s', $date['end']);
        $model->start = $start;
        $model->end = $end;


        $model->lat = $location['lat'];
        $model->lng = $location['lng'];
        $model->city = $location['city'];
        $model->place =$location['place'];
        $model->fullLocation = $location['place'].",".$location['city'];


        if ($model->load(Yii::$app->request->post())) {
            $validators = new Validators();
            if($_FILES['img']['error'] != 0){
                $result_of_base64 = $arr['img'];
            }
            else{
                $validate = $validators->eventValidator($_FILES['img']['type']);
                if($validate == true){
                }
                else if ($validate == false){
                    Yii::$app->getSession()->setFlash('error', 'Zły format zdjęcia! Obsługiwane formaty to jpg, jpeg, png oraz gif');
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
                else{
                    Yii::$app->getSession()->setFlash('error', 'Coś poszło nie tak z weryfikacją zdjęcia');
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
                $data = file_get_contents($_FILES['img']['tmp_name']);
                $result_of_base64 = base64_encode($data);
            }

            $split = explode(',', $model->fullLocation);
            $lenght = count($split);

            if ($lenght == 3 || $lenght == 2) {
                $model->city = $split[1];
                $model->place = $split[0];
            }elseif ($lenght >= 4) {
                $model->city = $split[2];
                $model->place = $split[0];
            }else{
                Yii::$app->getSession()->setFlash('error', 'Błędna lokalizacja');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            $array = [
                'name' => $model->name,
                'info' => $model->info,
                'img' => $result_of_base64,
                'location' =>[
                    'city' => $model->city,
                    'place' => $model->place,
                    'lat' => $model->lat,
                    'lng' => $model->lng
                ],
            ];

            if ($start != $model->start | $end != $model->end){
                $array['date'] = [
                    'start' => strtotime($model->start),
                    'end' => strtotime($model->end)
                ];

                $start = strtotime($model->start);
                $end = strtotime($model->end);

            }else{
                $array['date'] = [
                    'start' => $date['start'],
                    'end' => $date['end']
                ];
                $start = $date['start'];
                $end = $date['end'];

            }



            if($end == $start || $end < $start )
            {
                Yii::$app->getSession()->setFlash('error', 'Data zakończenia nie może poprzedzać daty rozpoczęcia');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


            if($end > strtotime('+3 day', $start)){
                Yii::$app->getSession()->setFlash('error', 'Czas trwania wydarzenia nie może być dłuższy od 3 dni');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


            $curl = $api->patch('event/'.$id, $array, $sid);
            if($curl->responseCode == 200){
                Yii::$app->getSession()->setFlash('success', 'Zaktualizowano wydarzenie');
                return $this->redirect('index');
            }elseif($curl->responseCode == 400){
                $curl_json = json_decode($curl->response,true);
                $message = $curl_json['errors'][0]['message'];
                if($message == "niepoprawny format daty"){
                    $curl_json = json_decode($curl->response,true);
                    $message = $curl_json['errors'][0]['message'];
                    if($message == "niepoprawny format daty"){
                        Yii::$app->getSession()->setFlash('error', 'Złe daty');
                    }
                    else {
                        Yii::$app->getSession()->setFlash('error', 'Złe dane');
                    }
                }
                else {
                    Yii::$app->getSession()->setFlash('error', 'Złe dane');
                }
            }else{
                Yii::$app->getSession()->setFlash('error', 'Błąd serwera');
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionViewEvent($id){

        $model = new EventsForm();
        //validation
        $validation = new CookiesController();
        $validation->CookiesValidation();
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');
        date_default_timezone_set('Europe/Warsaw');

            $api = new Api(Yii::$app->params['apiDomain']);
            $curl = $api->get('event/'.$id,[
                "Content-Type: application/json; charset=UTF-8",
                "Cookie: $sid",]);
           
            $json = $curl->response;
            $array = json_decode($json);
            $arr = json_decode(json_encode($array),true);
            $date = $arr['date'];
            $location = $arr['location'];

            $model->start = date('Y-m-d H:i:s',$date['start']);
            $model->end = date('Y-m-d H:i:s',$date['end']);
            $model->name = $arr['name'];
            $model->info = $arr['info'];
            $model->lat = $location['lat'];
            $model->lng = $location['lng'];
            $model->fullLocation = $location['place'].','.$location['city'];

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}