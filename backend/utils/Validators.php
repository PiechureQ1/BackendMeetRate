<?php
/**
 * Created by PhpStorm.
 * User: rst_user_1
 * Date: 25.07.2018
 * Time: 12:14
 */
namespace backend\utils;

class Validators
{

    public function eventValidator($imageType){
            if(!($imageType == 'image/jpg' || $imageType == 'image/png' || $imageType == 'image/gif'  || $imageType == 'image/jpeg' )){
                return false;
            }
            else{
                return true;
            }

    }
}