<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 12.07.2018
 * Time: 17:40
 */

namespace backend\models;


use yii\base\Model;

class TalksForm extends Model
{
    public $title;
    public $start;
    public $end;
    public $speakers;

    public function rules()
    {
        return [
            [['speakers','title', 'start', 'end'],'required'],
            [['title', 'start', 'end'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Tytuł',
            'start' => 'Początek',
            'end' => 'Zakończenie',
            'speakers' => 'Prelegent',
        ];
    }


}