<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 12.07.2018
 * Time: 17:40
 */

namespace backend\models;


use yii\base\Model;

class EventsForm extends Model
{
    public $name;
    public $img;
    public $fullLocation;
    public $city;
    public $place;
    public $lat;
    public $lng;
    public $start;
    public $end;
    public $info;

    public function rules()
    {
        return [
            [['start', 'end','info','fullLocation', 'name'], 'required', 'on'=>'create'],
            [['start', 'end','city,','place','lat','lng','info','fullLocation', 'name'],'string'],
            [['img'], 'file', 'extensions'=>'jpg, gif, png', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 1],

            [['name'], 'string', 'max'=>100],
            [['info'], 'string', 'max'=>500]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Nazwa wydarzenia',
            'start' => 'Początek',
            'end' => 'Zakończenie',
            'city' => 'Miasto',
            'place' => 'Miejsce',
            'info' => 'Opis',
            'fullLocation' => 'Lokalizacja',
            'img'=>'Zdjecia'
        ];
    }




}