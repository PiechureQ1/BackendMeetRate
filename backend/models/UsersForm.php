<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 12.07.2018
 * Time: 17:40
 */

namespace backend\models;


use yii\base\Model;

class UsersForm extends Model
{
    public $username;
    public $password;
    public $email;
    public $type;

    public function rules()
    {
        return [
            [['username', 'email','type', 'password'], 'required'],
            [['email'], 'email'],
            [['email'],'string', 'max'=>45],
            [['username'],'string', 'max'=>30],
            [['password'],'string'],
            [['type'],'integer', 'min'=>1, 'max'=>3]
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Nazwa użytkownika',
            'mail' => 'E-mail',
            'type' => 'Typ konta',
            'password' => 'Hasło'
        ];
    }


}